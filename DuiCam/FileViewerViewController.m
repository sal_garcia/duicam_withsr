//
//  FileViewerViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "FileViewerViewController.h"

@interface FileViewerViewController () {
	// table views
	UITableView *mainTableView;
	UITableView *screenshotsTableView;
	UITableView *savedClipsTableView;
	
	// data source arrays
	NSArray *screenshots;
	NSArray *savedClips;
	
	// globals to track current controller and table view
	UIViewController *currentViewController;
	UITableView *currentTableView;
}

@end

@implementation FileViewerViewController

#pragma mark - Utilities

- (BOOL)isIOS7 {
    return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0;
}


#pragma mark - Buttons

- (void)doneButtonPressed {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)editButtonPressed {
	if ([currentTableView isEditing]) {
		[currentTableView setEditing:NO animated:YES];
		currentViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed)];
	} else {
		[currentTableView setEditing:YES animated:YES];
		currentViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonPressed)];
	}
}

#pragma mark - File system methods 

- (NSArray *)listOfFilesInDocumentsSubdirectory:(NSString *)subdirectory {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *destinationPath = [[paths objectAtIndex:0] stringByAppendingString:[@"/" stringByAppendingString:subdirectory]];
	
	NSError* error = nil;
	NSArray* filesArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error];
	if(error != nil) {
		NSLog(@"Error in reading files: %@", [error localizedDescription]);
		return [NSArray array];
	}
	
	// sort by creation date
	NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[filesArray count]];
	for(NSString* file in filesArray) {
		NSString* filePath = [destinationPath stringByAppendingPathComponent:file];
		NSDictionary* properties = [[NSFileManager defaultManager]
									attributesOfItemAtPath:filePath
									error:&error];
		NSDate* modDate = [properties objectForKey:NSFileModificationDate];
		
		if(error == nil)
		{
			[filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
										   filePath, @"path",
										   modDate, @"lastModDate",
										   nil]];
		}
	}
	
	// sort using a block
	// order inverted as we want latest date first
	NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
							^(id path1, id path2)
							{
								// compare
								NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
														   [path2 objectForKey:@"lastModDate"]];
								return comp;
							}];
	
	// iterate over the sorted files and extract the paths
	NSMutableArray *returnedPaths = [[NSMutableArray alloc] init];
	for (NSDictionary *next in sortedFiles)
		[returnedPaths addObject:[next objectForKey:@"path"]];
	
	return returnedPaths;
}

#pragma mark - File name extractor 

- (NSString *)getFilenameFromPath:(NSString *)path {
	return [path substringFromIndex:[path rangeOfString:@"/" options:NSBackwardsSearch].location + 1];
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// prevent the table view from highlighting the selection
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	// take action according to which table view was interacted with 
	if (tableView == mainTableView) {
        // Calculate table view height based on OS.
        double navigationBarHeight = 64.0; // Guaranteed to be accurate.
        double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - ([self isIOS7] ? 0 : navigationBarHeight);
        
		if (indexPath.section == 0) {
			// intialize the screenshots array
			screenshots = [self listOfFilesInDocumentsSubdirectory:@"screenshots/"];
			
			// create the next view controller
			UIViewController *screenshotsController = [[UIViewController alloc] init];
			screenshotsController.title = @"Screenshots";
			
			// add the edit bar button item
			UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed)];
			screenshotsController.navigationItem.rightBarButtonItem = editButton;
			         
			// create the table view
			screenshotsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStylePlain];
			screenshotsTableView.delegate = self;
			screenshotsTableView.dataSource = self;
			
			// add the table view to the controller
			[screenshotsController.view addSubview:screenshotsTableView];
			
			// save the next controller and table view as globals for future use
			currentViewController = screenshotsController;
			currentTableView = screenshotsTableView;
			
			// display the controller
			[self pushViewController:screenshotsController animated:YES];
		} else {
			// initialize the saved clips array from user defaults
			savedClips = [[NSUserDefaults standardUserDefaults] stringArrayForKey:@"kSavedClips"];
			
			// create the next view controller
			UIViewController *savedClipsController = [[UIViewController alloc] init];
			savedClipsController.title = @"Saved clips";
			
			// add the edit bar button item
			UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed)];
			savedClipsController.navigationItem.rightBarButtonItem = editButton;
			
			// create the table view
			savedClipsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStylePlain];
			savedClipsTableView.delegate = self;
			savedClipsTableView.dataSource = self;
			
			// add the table to the controller
			[savedClipsController.view addSubview:savedClipsTableView];
			
			// save the next controller and table view as globals for future use
			currentViewController = savedClipsController;
			currentTableView = savedClipsTableView;
			
			// display the controller
			[self pushViewController:savedClipsController animated:YES];
		}
	} else if (tableView == screenshotsTableView) {
		// determine which image was selected
		NSString *selectedPath = [screenshots objectAtIndex:indexPath.row];
		
		// load the image into a UIImage
		NSData *imageData = [NSData dataWithContentsOfFile:selectedPath];
		UIImage *image = [UIImage imageWithData:imageData];
		
		// create a screenshot viewer and pass in the image
		ScreenshotViewerViewController *svvc = [[ScreenshotViewerViewController alloc] initWithImage:image];
		
		// present the screenshot viewer
		// [self presentModalViewController:svvc animated:YES];
        [self presentViewController:svvc animated:YES completion:nil];
	} else {
		// determine which video was selected
		NSString *selectedPath = [savedClips objectAtIndex:indexPath.row];
		
		// load the video in a player
		PlayerViewController *pvc = [[PlayerViewController alloc] initWithPathToClip:selectedPath];
		
		// hide the save button, this video is already saved 
		[pvc hideSaveButton];
		
		// present the player 
		// [self presentModalViewController:pvc animated:YES];
        [self presentViewController:pvc animated:YES completion:nil];
	}
}

#pragma mark - table view data source

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == screenshotsTableView) {
		// get the path of the file to be removed
		NSString *path = [screenshots objectAtIndex:indexPath.row];
		
		// delete the file
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
		
		// delete the entry from the array
		NSMutableArray *newScreenshots = [[NSMutableArray alloc] initWithArray:screenshots];
		[newScreenshots removeObjectAtIndex:indexPath.row];
		screenshots = [NSArray arrayWithArray:newScreenshots];
	} else {
		// get the path of the file to be removed
		NSString *path = [savedClips objectAtIndex:indexPath.row];
		
		// remove the path from the saved clips array in user defaults
		NSMutableArray *currentSavedClips = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] stringArrayForKey:@"kSavedClips"]];
		[currentSavedClips removeObject:path];
		
		// put the updated saved clips back into user defaults
		[[NSUserDefaults standardUserDefaults] setObject:currentSavedClips forKey:@"kSavedClips"];
		
		// determine if the clip is a temporary clip
		// get a list of all clips
		NSArray *allClips = [self listOfFilesInDocumentsSubdirectory:@"clips/"];
		
		// get the index of the clip in question and number of clips
		int indexOfClip = [allClips indexOfObject:path];
		int numClips = [allClips count];
		
		// check if the index of the clip in question is one of the last three indices, delete it if so 
		if (!(indexOfClip == numClips - 1 || indexOfClip == numClips - 2 || indexOfClip == numClips - 3))
			[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
		
		// delete the entry from the array
		NSMutableArray *newSavedClips = [[NSMutableArray alloc] initWithArray:savedClips];
		[newSavedClips removeObjectAtIndex:indexPath.row];
		savedClips = [NSArray arrayWithArray:newSavedClips];
	}
	
	// delete the entry from the appropriate table view
	[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewCellEditingStyleDelete;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == mainTableView) {
		// Dequeue or create a cell
		static NSString *identifer = @"mainTableViewReuseIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:identifer];
		}
		
		// add a chevron to the right side of the cell
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		
		// determine text of the cell based on section
		switch (indexPath.section) {
			case 0:
			{
				cell.textLabel.text = @"Saved screenshots";
			}
				break;
			case 1:
			{
				cell.textLabel.text = @"Saved clips";
			}
			default:
				break;
		}
		
		return cell;
	} else if (tableView == screenshotsTableView) {
		// dequeue or create a cell
		static NSString *identifier = @"screenshotsTableViewReuseIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		if (!cell)
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
		
		cell.textLabel.text = [self getFilenameFromPath:[screenshots objectAtIndex:indexPath.row]];
		
		return cell;
	} else {
		// dequeue or create a cell
		static NSString *identifier = @"screenshotsTableViewReuseIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		if (!cell)
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
		
		cell.textLabel.text = [self getFilenameFromPath:[savedClips objectAtIndex:indexPath.row]];
		
		return cell;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (tableView == mainTableView)
		return 2; // saved videos and screenshots directories
	else
		return 1; // just one list of files
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (tableView == mainTableView)
		return 1;
	else if (tableView == screenshotsTableView)
		return [screenshots count];
	else
		return [savedClips count];
		
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	// create the root view controller
	UIViewController *rootController = [[UIViewController alloc] init];
	rootController.title = @"Folders";
	
	// create a bar button item for the root view controller
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
	rootController.navigationItem.rightBarButtonItem = doneButton;
	
	// initialize the main table view
	mainTableView = [[UITableView alloc] initWithFrame:rootController.view.bounds style:UITableViewStyleGrouped];
	mainTableView.delegate = self;
	mainTableView.dataSource = self;
	
	// add the table view as a subview of the root controller
	[rootController.view addSubview:mainTableView];
	
	// make the root controller visible
	[self pushViewController:rootController animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
