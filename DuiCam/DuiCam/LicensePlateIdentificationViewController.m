//
//  LicensePlateIdentificationViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/26/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import "LicensePlateIdentificationViewController.h"

@interface LicensePlateIdentificationViewController ()

@end

@implementation LicensePlateIdentificationViewController

#pragma mark - Inits

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPathToClip:(NSString *)path andCurrentTime:(double)time {
	self = [super init];
	if (self) {
		pathToClip = path;
        currentTime = time;
	}
    
	return self;
}

#pragma mark - Touch handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    
    if (point.y > mainImageView.frame.size.height + mainImageView.frame.origin.y)
        point.y = mainImageView.frame.size.height + mainImageView.frame.origin.y;
    else if (point.y < mainImageView.frame.origin.y)
        point.y = mainImageView.frame.origin.y;
    
    [selectionView setHidden:NO];
    [selectionView setFrame:CGRectMake(point.x, point.y, 1, 1)];
    
    selectionRectCenter = point;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    
    if (point.y > mainImageView.frame.size.height + mainImageView.frame.origin.y)
        point.y = mainImageView.frame.size.height + mainImageView.frame.origin.y;
    else if (point.y < mainImageView.frame.origin.y)
        point.y = mainImageView.frame.origin.y;
    
    double x = selectionRectCenter.x;
    double y = selectionRectCenter.y;
    double width = 0;
    double height = 0;
    
    if (point.x > x) {
        width = point.x - x;
    } else {
        width = x - point.x;
        x = point.x;
    }
    if (point.y > y) {
        height = point.y - y;
    } else {
        height = y - point.y;
        y = point.y;
    }
    
    [selectionView setFrame:CGRectMake(x, y, width, height)];
}

#pragma mark - Notifications

- (void)videoFrameGenerated:(NSNotification *)notification {
    if (numFramesGenerated == totalNumFrames)
        return;
    
    numFramesGenerated++;
    UIImage *videoFrame = [notification.userInfo objectForKey:MPMoviePlayerThumbnailImageKey];
    NSNumber *frameTime = [notification.userInfo objectForKey:MPMoviePlayerThumbnailTimeKey];
    
    NSMutableDictionary *d = [NSMutableDictionary dictionaryWithObjectsAndKeys:videoFrame, @"frame", frameTime, @"time", nil];
    [videoFrames addObject:d];
    NSLog(@"Loaded %d", numFramesGenerated);
    
    if (numFramesGenerated == totalNumFrames) {
        NSSortDescriptor *sortByTime = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES];
        videoFrames = [NSMutableArray arrayWithArray:[videoFrames sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByTime]]];
        [mainImageView setImage:videoFrame];
        [frameNumberLabel setText:@"Frame 1"];
    }
}

- (void)videoReadyToPlay:(NSNotification *)notification {
    if ((player.loadState & MPMovieLoadStatePlaythroughOK) == MPMovieLoadStatePlaythroughOK) {
        const double FRAME_DURATION = .1;
        totalNumFrames = 10;
        
        NSMutableArray *frameTimes = [[NSMutableArray alloc] init];
        for (int i = 0; i < totalNumFrames; ++i) {
            [frameTimes addObject:[NSNumber numberWithDouble:(currentTime + (i * FRAME_DURATION))]];
        }
        
        NSLog(@"Num frames: %d", [frameTimes count]);
        
        videoFrames = [[NSMutableArray alloc] init];
        
        [player requestThumbnailImagesAtTimes:frameTimes timeOption:MPMovieTimeOptionExact];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:pathToClip]];
	player.movieSourceType = MPMovieSourceTypeFile;
	[player prepareToPlay];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoFrameGenerated:)
                                                 name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                               object:player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoReadyToPlay:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:player];

    numFramesGenerated = 0;
    currentFrame = 0;
    
    [selectionView.layer setBorderWidth:1.0];
    [selectionView.layer setBorderColor:[[UIColor redColor] CGColor]];
    
    [selectionView setFrame:CGRectMake(0, 0, 0, 0)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Image cropping

- (void)cropImageWithSuppress:(BOOL)suppress {
    if ([[videoFrames objectAtIndex:currentFrame] objectForKey:@"crop"]) {
        [[[videoFrames objectAtIndex:currentFrame] objectForKey:@"crop"] removeFromSuperview];
        [[videoFrames objectAtIndex:currentFrame] removeObjectForKey:@"crop"];
        [self displayFrame:currentFrame];
        return;
    }
    
    // Check if the user has selected a region.
    if (selectionView.frame.size.width == 0 && selectionView.frame.size.height == 0) {
        if (!suppress)
            [[[UIAlertView alloc] initWithTitle:@"Select a region" message:@"Select a region of the frame containing a license plate" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    // Calculate selection rectangle ratios.
    double x_ratio = selectionView.frame.origin.x / mainImageView.frame.size.width;
    double y_ratio = (selectionView.frame.origin.y - mainImageView.frame.origin.y) / mainImageView.frame.size.height;
    double width_ratio = selectionView.frame.size.width / mainImageView.frame.size.width;
    double height_ratio = selectionView.frame.size.height / mainImageView.frame.size.height;
    
    // Create the image from a png file
    UIImage *image = [[videoFrames objectAtIndex:currentFrame] objectForKey:@"frame"];
    
    // Get size of current image
    CGSize size = [image size];
    
    // Create rectangle that represents a cropped image
    // from the middle of the existing image
    CGRect rect = CGRectMake(size.width * x_ratio, size.height * y_ratio, size.width * width_ratio, size.height * height_ratio);
    
    // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    // Create and show the new image from bitmap data
    UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
    [imageView setFrame:CGRectMake(0, 0, selectionView.frame.size.width, selectionView.frame.size.height)];
    imageView.center = self.view.center;
    [self.view addSubview:imageView];
    
    mainImageView.hidden = YES;
    
    NSMutableDictionary *dict = [videoFrames objectAtIndex:currentFrame];
    [dict setObject:imageView forKey:@"crop"];
    
    [selectionView setHidden:YES];
    
    [self displayFrame:currentFrame];
    
    if ([self allFramesSelected]) {
        [doneButton setEnabled:YES];
    }
}

#pragma mark - Buttons

- (IBAction)cropButtonPressed:(id)sender {
    [self cropImageWithSuppress:NO];
}

- (IBAction)doneButtonPressed:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)displayFrame:(int)frameNumber {
    if ([[videoFrames objectAtIndex:frameNumber] objectForKey:@"crop"]) {
        mainImageView.hidden = YES;
        [[[videoFrames objectAtIndex:frameNumber] objectForKey:@"crop"] setHidden:NO];
        [cropButton setTitle:@"Reselect" forState:UIControlStateNormal];
        cropButton.hidden = NO;
    } else {
        mainImageView.hidden = NO;
        [mainImageView setImage:[[videoFrames objectAtIndex:frameNumber] objectForKey:@"frame"]];
        // [cropButton setTitle:@"Select" forState:UIControlStateNormal];
        cropButton.hidden = YES;
    }
    
    [frameNumberLabel setText:[NSString stringWithFormat:@"Frame %d", frameNumber + 1]];
    
    [selectionView setFrame:CGRectMake(0, 0, 0, 0)];
}

- (void)hideFrame:(int)frameNumber {
    if ([[videoFrames objectAtIndex:frameNumber] objectForKey:@"crop"]) {
        [[[videoFrames objectAtIndex:frameNumber] objectForKey:@"crop"] setHidden:YES];
    }
}

- (IBAction)previousFrameButtonPressed:(id)sender {
    // Crop the image if necessary.
    if (![[videoFrames objectAtIndex:currentFrame] objectForKey:@"crop"]) {
        [self cropImageWithSuppress:YES];
    }
    
    [self hideFrame:currentFrame];
    
    currentFrame -= 1;
    if (currentFrame < 0)
        currentFrame = 0;
    
    [self displayFrame:currentFrame];
}


- (IBAction)nextFrameButtonPressed:(id)sender {
    // Crop the image if necessary.
    if (![[videoFrames objectAtIndex:currentFrame] objectForKey:@"crop"]) {
        [self cropImageWithSuppress:YES];
    }
    
    [self hideFrame:currentFrame];
    
    if ([videoFrames count] == 0)
        return;
    
    currentFrame += 1;
    if (currentFrame >= [videoFrames count])
        currentFrame = [videoFrames count] - 1;

    [self displayFrame:currentFrame];
}

- (BOOL)allFramesSelected {
    for (NSMutableDictionary * next in videoFrames) {
        if (![next objectForKey:@"crop"])
            return NO;
    }
    return YES;
}

@end
