//
//  RecorderViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "FeedbackViewController.h"
#import "SaveButtonImageView.h"
#import "PlayerViewController.h"
#import <CoreLocation/CoreLocation.h>

@class ViewController;
@class SaveButtonImageView;

@interface RecorderViewController : UIViewController <AVCaptureFileOutputRecordingDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate, UIAlertViewDelegate> {
    #define MILES_PER_METER 0.000621371
    #define SECONDS_PER_HOUR 3600
    #define DIMMER_DELAY 15 // seconds
    
    #define METADATA_ALERT_VIEW 7
    #define BATTERY_ALERT_VIEW 8
    
    NSTimer *cancelRecordingTimer;
    int cancelTimerCount;
    UIAlertView *cancelAlertView;
    long lastBatteryNotification;
    UIDeviceBatteryState lastState;
    
	// Outlets
	IBOutlet UIView *cameraView;
	IBOutlet UIView *controlsContainerView;
	IBOutlet UIButton *dimmerButton;
	IBOutlet UILabel *dimmerLabel;
	IBOutlet UILabel *timestampLabel;
	__unsafe_unretained IBOutlet UIActivityIndicatorView *camcorderActivityIndicator;
    __unsafe_unretained IBOutlet UIButton *quickSaveButton;
    __unsafe_unretained IBOutlet SaveButtonImageView *quickSaveButtonImageView;
    __unsafe_unretained IBOutlet SaveButtonImageView *longSaveButtonImageView;
    __weak IBOutlet UIImageView *uploadButtonImageView;
    __unsafe_unretained IBOutlet UILabel *savingAllClipsLabel;
    __weak IBOutlet UILabel *speedLabel;
    
    NSString *pathToRecording;
    NSString *pathToLocationLog;
    
	// Clip length timer 
	NSTimer *clipLengthTimer;
	BOOL shouldStartNewRecording;
	
	// timestamp variables
	NSDate *recordingStartDate;
	NSTimer *timestampUpdateTimer;
	
	// AV Capture Variables
	AVCaptureSession *session;
	AVCaptureVideoPreviewLayer *previewLayer;
	AVCaptureMovieFileOutput *movieFileOutput;
	
	// Feedback view controller
	FeedbackViewController *fvc;
    
    // Camcorder warning label animation.
    BOOL camcorderWarningLabelAnimationEnabled;
    
    // Location manager.
    CLLocationManager *locationManager;
    NSMutableArray *pastLocations;
    NSTimer *locationTimer;
    
    BOOL shouldUploadClip;
}

// SaveButtonImageView delegate methods.
- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesBeganAtPoint:(UITouch *)touch;
- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesMovedAtPoint:(UITouch *)touch;
- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesEndedAtPoint:(UITouch *)touch;
- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesCancelledAtPoint:(UITouch *)touch;

// IBActions
- (IBAction)stopButtonPressed:(id)sender;
- (IBAction)brightnessButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)dimmerButtonPressed:(id)sender;
- (IBAction)camcorderButtonPressed:(id)sender;

@property ViewController *delegate;

@end
