//
//  LicensePlateIdentificationViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/26/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface LicensePlateIdentificationViewController : UIViewController {
    MPMoviePlayerController *player;
    
    NSString *pathToClip;
    double currentTime;
    
    __weak IBOutlet UIImageView *mainImageView;
    __weak IBOutlet UILabel *frameNumberLabel;
    __weak IBOutlet UIView *selectionView;
    __weak IBOutlet UIButton *cropButton;
    __weak IBOutlet UIBarButtonItem *doneButton;
    __weak IBOutlet UIButton *nextButton;
    __weak IBOutlet UIButton *prevButton;
    
    NSMutableArray *videoFrames;
    
    int numFramesGenerated;
    int totalNumFrames;
    int currentFrame;
    
    CGPoint selectionRectCenter;
}


- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)previousFrameButtonPressed:(id)sender;
- (IBAction)nextFrameButtonPressed:(id)sender;
- (IBAction)cropButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;

- (id)initWithPathToClip:(NSString *)path andCurrentTime:(double)time;

- (void)videoFrameGenerated:(NSNotification *)notification;
- (void)videoReadyToPlay:(NSNotification *)notification;

@end
