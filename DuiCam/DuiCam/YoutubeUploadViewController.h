//
//  YoutubeUploadViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/20/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerViewController;

#import "PlayerViewController.h"

@interface YoutubeUploadViewController : UIViewController {
    #define KEYBOARD_HEIGHT 216
    #define NAV_BAR_HEIGHT 60
    
    __weak IBOutlet UITextView *descriptionTextView;
    __weak IBOutlet UITextField *titleTextField;
    __weak IBOutlet UITextField *tagsTextField;
    __weak IBOutlet UIButton *uploadButton;
    __weak IBOutlet UIProgressView *uploadProgressView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet UISwitch *shareSwitch;
}

// IBActions.
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)uploadButtonPressed:(id)sender;

// Video upload progress.
- (void)updateVideoUploadProgressWithNumRead:(unsigned long long)numBytesRead andDataLength:(unsigned long long)dataLength;

@property PlayerViewController *uploadDelegate;

@end
