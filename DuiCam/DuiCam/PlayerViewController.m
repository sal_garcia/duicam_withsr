//
//  PlayerViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "PlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface PlayerViewController () {
	// path to the clip currently loaded into the player
	NSString *pathToClip;
	
    // Path to clip to be uploaded.
    NSString *pathToClipToBeUploaded;
    
	// the player itself
	MPMoviePlayerController *player;
	
	// timer that auto-hides the top bar
	NSTimer *topBarAutoHideTimer;
	
	// timestamp update timer
	NSTimer *timestampUpdateTimer;
	
	// slider update timer
	NSTimer *sliderUpdateTimer;
	
	// save button hiding flag
	BOOL hideSaveButton;
	
	// feedback view controller
	FeedbackViewController *fvc;
}

@end

@implementation PlayerViewController

#pragma mark - Playback Controls UI 

- (void)showPlayButton {
	[pausePlayButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
}

- (void)showPauseButton {
	[pausePlayButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];	
}

#pragma mark - Save button hiding

- (void)hideSaveButton {
	hideSaveButton = YES;
}

#pragma mark - Playback Control

- (void)adjustPlaybackByTime:(double)time {
	// get the current playback time
	double currentPlaybackTime = player.currentPlaybackTime;
	
	// calculate the new playback time by adding on our delta
	double newPlaybackTime = currentPlaybackTime + time;
	
	// check if we've gone outside the bounds of the video
	double clipDuration = player.duration;
	if (newPlaybackTime > clipDuration)
		newPlaybackTime = clipDuration;
	else if (newPlaybackTime < 0.0)
		newPlaybackTime = 0.0;
	
	// set the new playback time
	player.currentPlaybackTime = newPlaybackTime;
}

#pragma mark - Timestamp control 

- (void)updateTimestamp {
	int currentTime = player.currentPlaybackTime;
	
	// convert to hours/minutes/seconds
	int hours = currentTime / (60 * 60);
	currentTime = currentTime % (60 * 60);
	int minutes = currentTime / 60;
	currentTime = currentTime % 60;
	
	// display the time in the label
	timestampLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, currentTime];
}

- (void)startTimestampUpdateTimer {
	// if the timer is already running, shut it off
	if (timestampUpdateTimer)
		[timestampUpdateTimer invalidate];
	timestampUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(updateTimestamp) userInfo:nil repeats:YES];
}

#pragma mark - Top bar auto hiding 

- (void)hideTopBar {
	[UIView animateWithDuration:0.3
					 animations:^{
						 topBar.alpha = 0.0;
					 }];
}

- (void)showTopBar {
	[UIView animateWithDuration:0.3
					 animations:^{
						 topBar.alpha = 1.0;
					 }];
}

- (void)toggleTopBar {
	if (topBar.alpha < 0.5)
		[self showTopBar];
	else
		[self hideTopBar];
}

- (void)startTopBarAutoHideTimer {
	topBarAutoHideTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideTopBar) userInfo:nil repeats:NO];
}

#pragma mark - Email 

- (void)emailClipAtPath:(NSString *)path {
	// check if the user can send email
	if (![MFMailComposeViewController canSendMail]) {
		[[[UIAlertView alloc] initWithTitle:@"Error" message:@"This device is not currently set up to send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
		return;
	}

	// attach the video to the email and present it
	MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
	mailComposer.mailComposeDelegate = self;
	NSData *videoData = [NSData dataWithContentsOfFile:path];
	[mailComposer addAttachmentData:videoData mimeType:@"video/mp4" fileName:@"video.mp4"];
    
    // Attach the location log to the email if it exists.
    NSString *locationLogPath = [[[pathToClip substringToIndex:([pathToClip length] - 3)] stringByAppendingString:@"txt"] stringByReplacingOccurrencesOfString:@"/clips/" withString:@"/location_logs/"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:locationLogPath]) {
        NSData *locationLogData = [NSData dataWithContentsOfFile:locationLogPath];
        [mailComposer addAttachmentData:locationLogData mimeType:@"text/plain" fileName:@"location_log.txt"];
    }
    
    [self presentViewController:mailComposer animated:YES completion:Nil];
}

#pragma mark - Video editor delegate 

- (void)videoEditorController:(UIVideoEditorController *)editor didSaveEditedVideoToPath:(NSString *)editedVideoPath {
	// [self dismissModalViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
	
    if (clipTrimmingInitiatedFromEmail) {
        // email the edited clip
        [self emailClipAtPath:editedVideoPath];
    } else {
        pathToClipToBeUploaded = editedVideoPath;
        
        // Present the upload view controller.
        ytUploadController = [[YoutubeUploadViewController alloc] init];
        ytUploadController.uploadDelegate = self;
        [self presentViewController:ytUploadController animated:YES completion:nil];
    }
}

- (void)videoEditorControllerDidCancel:(UIVideoEditorController *)editor {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
	
	// screen goes black, so play and immediately pause to refresh it
	[player play];
	[player pause];
}

- (void)videoEditorController:(UIVideoEditorController *)editor didFailWithError:(NSError *)error {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
	
	// screen goes black, so play and immediately pause to refresh it
	[player play];
	[player pause];
}

#pragma mark - Mail composer delegate 

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
	
	// screen goes black, so play and immediately pause to refresh it
	[player play];
	[player pause];
}

#pragma mark - Alert View Delegate 

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == TRIM_ALERT_VIEW) {
        // 0 is cancel, 1 is trim
        if (buttonIndex == 1) {
            clipTrimmingInitiatedFromEmail = YES;
            const long maxSendSize = 15728640; // 15MB
            if ([UIVideoEditorController canEditVideoAtPath:pathToClip]) {
                long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:pathToClip error:nil] fileSize];
                double duration = [player duration];
                double maxLength = duration * maxSendSize / fileSize;
                UIVideoEditorController *trimmer = [[UIVideoEditorController alloc] init];
                [trimmer setDelegate:self];
                [trimmer setVideoQuality:UIImagePickerControllerQualityTypeHigh];
                [trimmer setVideoPath:pathToClip];
                [trimmer setVideoMaximumDuration:maxLength];
                [self presentViewController:trimmer animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Editing error" message:@"Video editor can't edit the current video" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    } else if (alertView.tag == UPLOAD_ALERT_VIEW) {
        if (buttonIndex == 1) {
            [self authenticateUser];
        }
    } else if (alertView.tag == DUICAM_SIGNIN_ALERT_VIEW) {
        if (buttonIndex == 1) {
            DuicamOrgLoginViewController *duicamLogin = [[DuicamOrgLoginViewController alloc] init];
            [self presentViewController:duicamLogin animated:YES completion:nil];
        }
    } else if (alertView.tag == UPLOAD_TRIM_ALERT_VIEW) {
        // 1 is trim, 0 is don't trim.
        clipTrimmingInitiatedFromEmail = NO;
        if (buttonIndex == 1) {
            if ([UIVideoEditorController canEditVideoAtPath:pathToClip]) {
                UIVideoEditorController *trimmer = [[UIVideoEditorController alloc] init];
                [trimmer setDelegate:self];
                [trimmer setVideoQuality:UIImagePickerControllerQualityTypeHigh];
                [trimmer setVideoPath:pathToClip];
                [self presentViewController:trimmer animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Editing error" message:@"Video editor can't edit the current video" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        } else {
            // Present the upload view controller.
            ytUploadController = [[YoutubeUploadViewController alloc] init];
            ytUploadController.uploadDelegate = self;
            [self presentViewController:ytUploadController animated:YES completion:nil];
        }
    }
}

#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// 0 is email, 1 is photo roll, 2 is upload to duicam.org, 3 is cancel
	switch (buttonIndex) {
		case 0:
		{
			// email the clip
			// first check if it's too large to email
			NSFileManager *fileManager = [NSFileManager defaultManager];
			long fileSize = [[fileManager attributesOfItemAtPath:pathToClip error:nil] fileSize];
			long maxSendSize = 15 * 1024 * 1024; // 15MB
			if (fileSize > maxSendSize) {
				// trim the clip
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Clip is too large" message:@"This clip is over 15MB, and is too large to send. Would you like to trim the clip?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Trim", nil];
                alertView.tag = TRIM_ALERT_VIEW;
                [alertView show];
			} else {
				// email the clip
				[self emailClipAtPath:pathToClip];
			}
		}
			break;
		case 1:
		{
			// save to photo roll
			UISaveVideoAtPathToSavedPhotosAlbum(pathToClip, nil, nil, nil);
		}
			break;
        case 2:
        {
            // Check if the user is already authenticated.
            static NSString *const kKeychainItemName = KEYCHAIN_ITEM_NAME;
            NSString *kMyClientID = CLIENT_ID;     // pre-assigned by service
            NSString *kMyClientSecret = CLIENT_SECRET; // pre-assigned by service
            
            GTMOAuth2Authentication *auth;
            auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                         clientID:kMyClientID
                                                                     clientSecret:kMyClientSecret];
            
            // Check if user is logged in to Duicam.org.
            NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:DUICAM_USERNAME_KEY];
            if (!email) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"You must be signed in" message:@"You must be signed in to Duicam.org to upload a video." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sign in", nil];
                alertView.tag = DUICAM_SIGNIN_ALERT_VIEW;
                [alertView show];
                return;
            }
            
            if (auth) {
                [self collectUploadInformationWithAuth:auth];
                return;
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Upload to YouTube" message:@"Your video will first be uploaded to YouTube, and will then be available on duicam.org. Please sign in to your YouTube account." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            alertView.tag = UPLOAD_ALERT_VIEW;
            [alertView show];
        }
		default:
			break;
	}
}

#pragma mark - Google APIs

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    if (error != nil) {
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self collectUploadInformationWithAuth:auth];
}

- (void)authenticateUser {
    // Constants used for storing credentials in keychain.
    static NSString *const kKeychainItemName = KEYCHAIN_ITEM_NAME;
    NSString *kMyClientID = CLIENT_ID;     // pre-assigned by service
    NSString *kMyClientSecret = CLIENT_SECRET; // pre-assigned by service
    
    // Authenticate the user.
    NSString *scope = kGTLAuthScopeYouTube; // scope for YouTube API
    GTMOAuth2ViewControllerTouch *viewController;
    viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:scope
                                                                clientID:kMyClientID
                                                            clientSecret:kMyClientSecret
                                                        keychainItemName:kKeychainItemName
                                                                delegate:self
                                                        finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    
    // Add a cancel button to the modal view.
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAuth)];
    [[viewController navigationItem] setLeftBarButtonItem:cancelButton];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)cancelAuth {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)collectUploadInformationWithAuth:(GTMOAuth2Authentication *)auth {
    // Store the auth for later.
    ytAuth = auth;
    
    pathToClipToBeUploaded = pathToClip;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    long fileSize = [[fileManager attributesOfItemAtPath:pathToClip error:nil] fileSize];
    long maxSendSize = 15 * 1024 * 1024; // 15MB
    if (fileSize > maxSendSize) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Large clip" message:@"This clip is large, and may take a long time to upload. Would you like to trim the clip?" delegate:self cancelButtonTitle:@"Don't trim" otherButtonTitles:@"Trim", nil];
        alertView.tag = UPLOAD_TRIM_ALERT_VIEW;
        [alertView show];
        return;
    }
    
    // Present the upload view controller.
    ytUploadController = [[YoutubeUploadViewController alloc] init];
    ytUploadController.uploadDelegate = self;
    [self presentViewController:ytUploadController animated:YES completion:nil];
}

- (void)uploadVideoWithTitle:(NSString *)title andDescription:(NSString *)description andTags:(NSArray *)tags andShare:(BOOL)share {
    NSString *path = pathToClipToBeUploaded;
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:path];
    
    if (!handle) {
        NSLog(@"Failed to open file for reading");
        return;
    }
    
    GTLServiceYouTube *service      = [[GTLServiceYouTube alloc] init];
    service.authorizer              = ytAuth;
    
    GTLUploadParameters *params     = [GTLUploadParameters uploadParametersWithFileHandle:handle MIMEType:@"application/octet-stream"];
    
    GTLYouTubeVideoSnippet *snippet = [GTLYouTubeVideoSnippet object];
    snippet.title                   = title;
    snippet.descriptionProperty     = description;
    snippet.tags                    = tags;
    snippet.categoryId              = @"17";
    
    GTLYouTubeVideoStatus *status   = [GTLYouTubeVideoStatus object];
    status.privacyStatus            = @"unlisted";
    
    GTLYouTubeVideo *video          = [GTLYouTubeVideo object];
    video.snippet                   = snippet;
    video.status                    = status;
    
    GTLQueryYouTube *query          = [GTLQueryYouTube queryForVideosInsertWithObject:video part:@"snippet,status" uploadParameters:params];
    
    // Perform the upload
    GTLServiceTicket *ticket        = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error)
                                       {
                                           if (error)
                                           {
                                               NSLog(@"ERROR: %@", error);
                                               [self dismissViewControllerAnimated:YES completion:nil];
                                               return;
                                           }
                                           
                                           NSLog(@"SUCCESS! %@; %@;", ticket, object);
                                           NSString *videoId = [ticket.fetchedObject.JSON objectForKey:@"id"];
                                           
                                           [self dismissViewControllerAnimated:YES completion:^{
                                               [fvc setTitle:@"Uploaded" andDescription:@"Clip successfully uploaded"];
                                               [fvc checkAndFade];
                                           }];
                                           
                                           [self uploadToDuicamOrg:videoId andShare:share];
                                       }];
    
    ticket.uploadProgressBlock = ^(GTLServiceTicket *ticket, unsigned long long numberOfBytesRead, unsigned long long dataLength) {
        [ytUploadController updateVideoUploadProgressWithNumRead:numberOfBytesRead andDataLength:dataLength];
    };
}

#pragma mark - DuiCam.org

- (void)uploadToDuicamOrg:(NSString *)videoId andShare:(BOOL)share {
    NSString *ytLink = [NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@", videoId];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSDictionary *parameters = @{ @"link" : ytLink,
                                  @"share" : [NSNumber numberWithBool:share] };

    [manager POST:[DUICAM_ORG_URL stringByAppendingString:@"json/youtube_links/submit"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

#pragma mark - Buttons

- (IBAction)saveButtonPressed:(id)sender {
	// display fvc
	[fvc setTitle:@"Saved" andDescription:@"Clip saved to folders"];
	[fvc checkAndFade];
	
	// get list of saved clips
	NSArray *savedClips = [[NSUserDefaults standardUserDefaults] stringArrayForKey:@"kSavedClips"];
	if (!savedClips)
		savedClips = [[NSArray alloc] init];
	
	// check if this clip is already saved
	// if so, just return
	if ([savedClips indexOfObject:pathToClip] != NSNotFound)
		return;
	
	// build a mutable array to add the new path to
	NSMutableArray *newSavedClips = [[NSMutableArray alloc] initWithArray:savedClips];
	
	// add the path of the current video to the end
	[newSavedClips addObject:pathToClip];
	
	// put the array back into user defaults
	[[NSUserDefaults standardUserDefaults] setObject:newSavedClips forKey:@"kSavedClips"];
}

- (IBAction)minusOneButtonPressed:(id)sender {
	[self adjustPlaybackByTime:-1.1]; // this makes it almost certain that we'll get to a new keyframe
	
	// update the slider, even if the timer has been invalidated
	[self updateSlider];
}

- (IBAction)rewindButtonPressed:(id)sender {
	// show the pause button
	[self showPauseButton];
	
	// get the current playback rate
	double currentPlaybackRate = player.currentPlaybackRate;
	
	// if the player isn't already rewinding, set the playback rate to -2
	double newPlaybackRate = -2.0;
	
	// if the player is already rewinding, double the current speed
	if (currentPlaybackRate < 0.0)
		newPlaybackRate = 2 * currentPlaybackRate;
	
	// set the new playback rate
	player.currentPlaybackRate = newPlaybackRate;
	
	// start the slider update timer
	[self startSliderUpdateTimer];
}

- (IBAction)pausePlayButtonPressed:(id)sender {
	// get the current state of playback
	MPMoviePlaybackState playbackState = [player playbackState];
	if (playbackState == MPMoviePlaybackStatePlaying || playbackState == MPMoviePlaybackStateSeekingForward || playbackState == MPMoviePlaybackStateSeekingBackward) {
		// player is playing, so pause it
		[player pause];
		// showing the play button is taken care of by the observer
	} else {
		// set the playback rate to normal
		player.currentPlaybackRate = 1.0;
		
		// player is paused, so play it
		[player play];
		
		// show the pause button
		[self showPauseButton];
		
		// start the top bar auto hide timer
		[self startTopBarAutoHideTimer];
		
		// start the slider update timer
		[self startSliderUpdateTimer];
	}
}

- (IBAction)forwardButtonPressed:(id)sender {
	// show the pause button
	[self showPauseButton];
	
	// get the current playback rate
	double currentPlaybackRate = player.currentPlaybackRate;
	
	// if the player isn't already fast forwarding, set the playback rate to 2
	double newPlaybackRate = 2.0;
	
	// if the player is already fast forwarding, double the current speed
	if (currentPlaybackRate > 1.0)
		newPlaybackRate = 2 * currentPlaybackRate;
	
	// set the new playback rate
	player.currentPlaybackRate = newPlaybackRate;
	
	// start the slider update timer
	[self startSliderUpdateTimer];
}

- (IBAction)plusOneButtonPressed:(id)sender {
	[self adjustPlaybackByTime:1.1]; // this makes it almost certain that we'll get to a new keyframe
	
	// update the slider, even if the timer has been invalidated
	[self updateSlider];
}

- (IBAction)screenshotButtonPressed:(id)sender {
	// display the fvc
	[fvc setTitle:@"Saving..." andDescription:@""];
	[fvc startAnimating];
	
	dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		// get current playback time
		double currentPlaybackTime = player.currentPlaybackTime;
		
		// get the screenshot at the current time
		UIImage *screenshot = [player thumbnailImageAtTime:currentPlaybackTime timeOption:MPMovieTimeOptionExact];
		
		// get the start date of this clip from user defaults
		NSDate *startDate = [[NSUserDefaults standardUserDefaults] objectForKey:[@"kClipBegin" stringByAppendingString:pathToClip]];
		
		// calculate the date of the screenshot
		NSDate *screenshotDate = [startDate dateByAddingTimeInterval:currentPlaybackTime];
		
		// create and set up a date formatter to output the filename
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		formatter.dateFormat = @"MM-dd-YY_HH:mm:ss";
		
		// output the filename from the formatter
		NSString *filename = [formatter stringFromDate:screenshotDate];
		
		// append the file type
		filename = [filename stringByAppendingString:@".png"];
		
		// calculate the full path
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *screenshotsPath = [[paths objectAtIndex:0] stringByAppendingString:@"/"];
		screenshotsPath = [screenshotsPath stringByAppendingString:@"screenshots/"];
		NSString *filepath = [screenshotsPath stringByAppendingString:filename];
		
		// check if the screenshot already exists. If it does, just return
		NSFileManager *fileManager = [NSFileManager defaultManager];
		if ([fileManager fileExistsAtPath:filepath]) {
			dispatch_async( dispatch_get_main_queue(), ^{
				// fade the fvc
				[fvc setTitle:@"Saved" andDescription:@"Screenshot saved to folders"];
				[fvc checkAndFade];
			});
			
			return;
		}
		
		// save the image
		NSData *screenshotData = UIImagePNGRepresentation(screenshot);
		[screenshotData writeToFile:filepath atomically:YES];
		
		dispatch_async( dispatch_get_main_queue(), ^{
			// fade the fvc
			[fvc setTitle:@"Saved" andDescription:@"Screenshot saved to folders"];
			[fvc checkAndFade];
		});
	});
}

- (IBAction)doneButtonPressed:(id)sender {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareButtonPressed:(id)sender {
	// pause the video
	[player pause];
	
	// display the action sheet with options
	UIActionSheet *shareOptions = [[UIActionSheet alloc] initWithTitle:@"Share Clip" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"Save to photo roll", @"Share on duicam.org", nil];
	[shareOptions showFromRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 1) inView:self.view animated:YES];
}

- (IBAction)tapInterceptorPressed:(id)sender {
	[self toggleTopBar];
}

- (IBAction)identifyLicensePlatesButtonPressed:(id)sender {
    [player pause];
    
    LicensePlateIdentificationViewController *lpivc = [[LicensePlateIdentificationViewController alloc] initWithPathToClip:pathToClip andCurrentTime:player.currentPlaybackTime];
    
    [self presentViewController:lpivc animated:YES completion:nil];
}

#pragma mark - Slider

- (void)updateSlider {
	// get the current playback time
	double currentTime = player.currentPlaybackTime;
	
	// calculate the ratio of the current time to the ending time
	double ratio = currentTime / player.duration;
	
	// update the slider
	scrubberSlider.value = ratio;
}

- (void)startSliderUpdateTimer {
	// if the timer is already running, shut it off 
	if (sliderUpdateTimer)
		[sliderUpdateTimer invalidate];
	sliderUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
}

- (IBAction)sliderMoved:(id)sender {
	// stop the timer from auto updating the slider
	[sliderUpdateTimer invalidate];
	
	// pause the video, let the user control it
	[player pause];
	
	// calculate the new current playback time
	double newPlaybackTime = scrubberSlider.value * player.duration;
	
	// set the new current playback time
	player.currentPlaybackTime = newPlaybackTime;
}


#pragma mark - Playback notifications

- (void)playbackStateDidChange {
	// if the video reaches the beginning or end, we need to update the play/pause button to the play symbol 
	
	MPMoviePlaybackState currentState = player.playbackState;
	if (currentState == MPMoviePlaybackStatePaused) {
		[self showPlayButton];
		[self showTopBar];
	}
}

#pragma mark - View Lifecycle 

- (id)initWithPathToClip:(NSString *)path {
	self = [super init];
	if (self) {
		pathToClip = path;
        pathToClipToBeUploaded = path;
	}

	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Adjust the UI on 3.5 inch screens.
    double screenHeight = [[UIScreen mainScreen] bounds].size.height * 2;
    const double SCREEN_HEIGHT_DIFFERENCE = 88;
    if (screenHeight <= 1000) {
        for (UIView *next in [self.view subviews]) {
            if ([next isKindOfClass:[UIButton class]] || [next isKindOfClass:[UILabel class]] || [next isKindOfClass:[UISlider class]]) {
                CGPoint currentCenter = next.center;
                next.center = CGPointMake(currentCenter.x, currentCenter.y - SCREEN_HEIGHT_DIFFERENCE);
            }
        }
            
    }
    
	// initialize the player
	player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:pathToClip]];
	player.movieSourceType = MPMovieSourceTypeFile;
	[player prepareToPlay];
	
	// configure the player
	player.view.frame = self.view.bounds;
	player.controlStyle = MPMovieControlStyleNone;
	
	// register for notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStateDidChange) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
	
	// display the player
	[self.view insertSubview:player.view atIndex:0];
	
	// hide the save button if necessary
	if (hideSaveButton)
		[saveButton setHidden:YES];
	
	// start the top bar auto hide timer
	[self startTopBarAutoHideTimer];
	
	// start the timestamp update timer
	[self startTimestampUpdateTimer];
	
	// start the slider update timer
	[self startSliderUpdateTimer];
	
	// set up the fvc
	fvc = [[FeedbackViewController alloc] init];

    fvc.view.frame = CGRectMake(0, 0, 200, 200);
	fvc.view.center = CGPointMake(160, 240);
	[self.view addSubview:fvc.view];
    
    clipTrimmingInitiatedFromEmail = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	pausePlayButton = nil;
	topBar = nil;
	timestampLabel = nil;
	scrubberSlider = nil;
	[timestampUpdateTimer invalidate];
	[sliderUpdateTimer invalidate];
	saveButton = nil;
	[super viewDidUnload];
}
#pragma mark - Interface Orientation

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskPortrait;
}

@end
