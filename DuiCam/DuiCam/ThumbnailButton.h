//
//  ThumbnailButton.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/20/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbnailButton : UIButton

@property NSString *path;

@end
