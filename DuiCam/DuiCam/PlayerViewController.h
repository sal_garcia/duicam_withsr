//
//  PlayerViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "FeedbackViewController.h"
#import <AFURLSessionManager.h>
#import "GTLYouTube.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "YoutubeUploadViewController.h"
#import "AFHTTPRequestOperationManager.h"

#import "DuicamOrgLoginViewController.h"
#import "LicensePlateIdentificationViewController.h"
#import "constants.h"

@class YoutubeUploadViewController;

@interface PlayerViewController : UIViewController <UIActionSheetDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, UIVideoEditorControllerDelegate, UINavigationControllerDelegate> {
	// IBOutlets 
	IBOutlet UIButton *pausePlayButton;
	IBOutlet UINavigationBar *topBar;
	IBOutlet UILabel *timestampLabel;
	IBOutlet UISlider *scrubberSlider;
	IBOutlet UIButton *saveButton;
    
    #define CLIENT_ID @"683781123527-vvifu4p6rap6qt5qgh000g8j6j1pmc61.apps.googleusercontent.com"
    #define CLIENT_SECRET @"JOUjhplqgxGGYNTglPBN6l0b"
    #define KEYCHAIN_ITEM_NAME @"DuiCam: YouTube"
    
    #define DUICAM_ORG_URL @"http://ec2-23-20-103-249.compute-1.amazonaws.com/"
    #define DUICAM_KEYCHAIN_ITEM_NAME @"DuiCam.org keychain"
    
    #define TRIM_ALERT_VIEW 0
    #define UPLOAD_ALERT_VIEW 1
    #define DUICAM_SIGNIN_ALERT_VIEW 2 
    #define UPLOAD_TRIM_ALERT_VIEW 3
    
    BOOL clipTrimmingInitiatedFromEmail;
    
    YoutubeUploadViewController *ytUploadController;
    GTMOAuth2Authentication *ytAuth;
}

// inits
- (id)initWithPathToClip:(NSString *)path;

// save button hiding
- (void)hideSaveButton;

// IBActions
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)minusOneButtonPressed:(id)sender;
- (IBAction)rewindButtonPressed:(id)sender;
- (IBAction)pausePlayButtonPressed:(id)sender;
- (IBAction)forwardButtonPressed:(id)sender;
- (IBAction)plusOneButtonPressed:(id)sender;
- (IBAction)screenshotButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)shareButtonPressed:(id)sender;
- (IBAction)tapInterceptorPressed:(id)sender;
- (IBAction)sliderMoved:(id)sender;
- (IBAction)identifyLicensePlatesButtonPressed:(id)sender;

// Video uploading.
- (void)uploadVideoWithTitle:(NSString *)title andDescription:(NSString *)description andTags:(NSArray *)tags andShare:(BOOL)share;

- (void)uploadToDuicamOrg:(NSString *)videoId andShare:(BOOL)share;

@end
