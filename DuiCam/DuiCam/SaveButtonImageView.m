//
//  SaveButtonImageView.m
//  DuiCam
//
//  Created by Daniel de Haas on 11/3/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "SaveButtonImageView.h"

@implementation SaveButtonImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [_touchDelegate saveButtonImageView:self touchesBeganAtPoint:touch];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [_touchDelegate saveButtonImageView:self touchesMovedAtPoint:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [_touchDelegate saveButtonImageView:self touchesEndedAtPoint:touch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [_touchDelegate saveButtonImageView:self touchesCancelledAtPoint:touch];
}

@end
