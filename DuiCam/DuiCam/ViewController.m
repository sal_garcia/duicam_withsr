//
//  ViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
	#define kLastUsedPresetKey @"kLastUsedPresetKey"
	#define kClipLengthKey @"kClipLengthKey"
	
	// clip display buttons
	NSMutableArray *clipDisplayButtons;
	
	// Feedback view controller
	FeedbackViewController *fvc;
	
	// Auto start vars
	UIAlertView *autoStartAlertView;
	NSTimer *autoStartCountdownTimer;
	NSTimer *autoStartTimer;
}

@end

@implementation ViewController

#pragma mark - Utilities

- (BOOL)is4InchScreen {
    return [[UIScreen mainScreen] bounds].size.height == 568;
}

#pragma mark - Recording

- (void)startNewRecording {
	RecorderViewController *rvc = [[RecorderViewController alloc] init];
	rvc.delegate = self;
	// [self presentModalViewController:rvc animated:YES];
    [self presentViewController:rvc animated:YES completion:nil];
}

#pragma mark - Clip Display

- (void)refreshClipDisplays {
	// remove all current clip displays
	[clipDisplayButtons removeAllObjects];
	
	// get list of clips to display
	NSArray *clipPathsDict = [self getOrderedClipFilepaths];
	NSMutableArray *clipPaths = [[NSMutableArray alloc] init];
	for (NSDictionary *next in clipPathsDict)
		[clipPaths addObject:[next objectForKey:@"path"]];

	// get rid of all clips but the last three
	while ([clipPaths count] > 3)
		[clipPaths removeObjectAtIndex:0];
	
	// create displays for the clips
	for (NSString *path in clipPaths)
		[self addNewClipDisplayFromClipAtPath:path withDelay:NO];
}

- (void)addNewClipDisplayFromClipAtPath:(NSString *)path withDelay:(BOOL)delay {
	const double THUMBNAIL_WIDTH = 104;
	const double THUMBNAIL_CENTER_Y = 300;
	const double PADDING = 2; // padding between two clip displays 
	
	// holds the new centers of all clip displays
	NSMutableArray *clipDisplayCenters = [[NSMutableArray alloc] init];
	
	// the thumbnail button (if any) to be destroyed when the animation completes
	ThumbnailButton *queuedForDeletion = nil;
	
	// calculate centers for all the thumbnails 
	if ([clipDisplayButtons count] >= 3) {
		// there are three clip displays now, so one has to be removed
		queuedForDeletion = [clipDisplayButtons objectAtIndex:0];
		
		// first clip has to be sent off screen
		CGPoint offscreen = CGPointMake(-THUMBNAIL_WIDTH, THUMBNAIL_CENTER_Y);
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:offscreen]];
		
		// next three clips should be in the same place as before
		for (ThumbnailButton *next in clipDisplayButtons)
			[clipDisplayCenters addObject:[NSValue valueWithCGPoint:next.center]];
	} else if ([clipDisplayButtons count] == 2) {
		CGPoint firstCenter = CGPointMake(PADDING + THUMBNAIL_WIDTH / 2, THUMBNAIL_CENTER_Y);
		CGPoint secondCenter = CGPointMake(firstCenter.x + PADDING + THUMBNAIL_WIDTH, THUMBNAIL_CENTER_Y);
		CGPoint thirdCenter = CGPointMake(secondCenter.x + PADDING + THUMBNAIL_WIDTH, THUMBNAIL_CENTER_Y);
		
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:firstCenter]];
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:secondCenter]];
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:thirdCenter]];
	} else if ([clipDisplayButtons count] == 1) {
		const double BORDER_PADDING = 55;
		
		CGPoint firstCenter = CGPointMake(BORDER_PADDING + THUMBNAIL_WIDTH / 2, THUMBNAIL_CENTER_Y);
		CGPoint secondCenter = CGPointMake(firstCenter.x + PADDING + THUMBNAIL_WIDTH, THUMBNAIL_CENTER_Y);
		
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:firstCenter]];
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:secondCenter]];
	} else {
		const double BORDER_PADDING = 108;
		
		CGPoint firstCenter = CGPointMake(BORDER_PADDING + THUMBNAIL_WIDTH / 2, THUMBNAIL_CENTER_Y);
		
		[clipDisplayCenters addObject:[NSValue valueWithCGPoint:firstCenter]];
	}
	
	// create the new ThumbnailButton
	ThumbnailButton *newClipDisplay = [ThumbnailButton buttonWithType:UIButtonTypeCustom];
		// set the path
		newClipDisplay.path = path;
		// set the thumbnail image
		[newClipDisplay setBackgroundImage:[self generateThumbnailForClipAtPath:path] forState:UIControlStateNormal];
		// set the size/position
		newClipDisplay.frame = CGRectMake(0, 0, THUMBNAIL_WIDTH, THUMBNAIL_WIDTH);
		newClipDisplay.center =CGPointMake([UIScreen mainScreen].bounds.size.width + THUMBNAIL_WIDTH / 2, THUMBNAIL_CENTER_Y); // start it offscreen
		// set up the timestamp label
		newClipDisplay.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
		newClipDisplay.titleLabel.textAlignment = UITextAlignmentCenter;
		newClipDisplay.titleLabel.font = [UIFont fontWithName:@"Courier" size:9];
		[newClipDisplay setTitle:[self getTimestampTextForFileAtPath:path] forState:UIControlStateNormal];
		newClipDisplay.titleEdgeInsets = UIEdgeInsetsMake(0, 0, THUMBNAIL_WIDTH * 6.0 / 5.0, 0);
		// add button target
		[newClipDisplay addTarget:self action:@selector(thumbnailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	// add the new thumbnail button to the array and view
	[clipDisplayButtons addObject:newClipDisplay];
	[self.view addSubview:newClipDisplay];
	
	// animate all clip displays to their new positions
	[UIView animateWithDuration:0.5
						  delay:(delay ? 0.5 : 0)
						options:0
					 animations:^{
						 for (int i = 0; i < [clipDisplayButtons count]; ++i) {
							 CGPoint nextCenter = [[clipDisplayCenters objectAtIndex:i] CGPointValue];
							 [[clipDisplayButtons objectAtIndex:i] setCenter:nextCenter];
						 }
					 }
					 completion:^(BOOL finished) {
						 if (queuedForDeletion)
							 [clipDisplayButtons removeObject:queuedForDeletion];
					 }];
}

- (NSString *)getTimestampTextForFileAtPath:(NSString *)path {
	// get the begin and end dates from user defaults
	NSDate *beginDate = [[NSUserDefaults standardUserDefaults] objectForKey:[@"kClipBegin" stringByAppendingString:path]];
	NSDate *endDate = [[NSUserDefaults standardUserDefaults] objectForKey:[@"kClipEnd" stringByAppendingString:path]];
	
	// set up a date formatter for the timestamp
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	formatter.dateFormat = @"HH:mm:ss";
	
	// output the timestamp
	NSString *timestamp = [formatter stringFromDate:beginDate];
	if (endDate) {
		timestamp = [timestamp stringByAppendingString:@"-"];
		timestamp = [timestamp stringByAppendingString:[formatter stringFromDate:endDate]];
	}
	
	// set up the formatter to output a date
	formatter.dateFormat = @"MM/dd/yy";
	
	// output the rest of the timestamp
	timestamp = [timestamp stringByAppendingString:@"\n"];
	timestamp = [timestamp stringByAppendingString:[formatter stringFromDate:beginDate]];
	
	return timestamp;
}

- (UIImage *)generateThumbnailForClipAtPath:(NSString *)path {
	NSURL *pathURL = [NSURL fileURLWithPath:path];
	MPMoviePlayerController *thumbnailExtractor = [[MPMoviePlayerController alloc] initWithContentURL:pathURL];
	UIImage *thumbnail = [thumbnailExtractor thumbnailImageAtTime:0 timeOption:MPMovieTimeOptionNearestKeyFrame]; // this UIImage is the thumbnail for the new video
	return thumbnail;
}

// Returns paths to all files in the clips directory, sorted oldest to newest.
- (NSArray *)getOrderedClipFilepaths {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/clips/"];
	
	NSError* error = nil;
	NSArray* filesArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error];
	if (error != nil) {
		NSLog(@"Error in reading files: %@", [error localizedDescription]);
        return [NSArray array];
	}
	
	// sort by creation date
	NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[filesArray count]];
	for(NSString* file in filesArray) {
		NSString* filePath = [destinationPath stringByAppendingPathComponent:file];
		NSDictionary* properties = [[NSFileManager defaultManager]
									attributesOfItemAtPath:filePath
									error:&error];
		NSDate* modDate = [properties objectForKey:NSFileModificationDate];
		
		if(error == nil) {
			[filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
										   filePath, @"path",
										   modDate, @"lastModDate",
										   nil]];
		}
	}
	
    // sort using a block
    // order inverted as we want latest date first
	NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
							^(id path1, id path2)
							{
								// compare
								NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
														   [path2 objectForKey:@"lastModDate"]];
								return comp;
							}];
	
	return sortedFiles;
}

#pragma mark - Recorder Delegate Methods

- (void)newClipSavedAtPath:(NSString *)path {
	[self addNewClipDisplayFromClipAtPath:path withDelay:YES];
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	// 0 is "Stop", 1 is "Start now"
	// shut off the countdown timers
	[autoStartCountdownTimer invalidate];
	[autoStartTimer invalidate];
	if (buttonIndex == 1)
		[self autoStart];
}

#pragma mark - Recording auto start

- (void)promptToAutoStart {
	// prompt user with the alert view
	autoStartAlertView = [[UIAlertView alloc] initWithTitle:@"Recording will begin" message:@"The recording will automatically begin in 10 seconds" delegate:self cancelButtonTitle:@"Stop" otherButtonTitles:@"Start now", nil];
	[autoStartAlertView show];
	
	// set up the timer that makes the alert view count down 
	autoStartCountdownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateAutoStartCountdown) userInfo:nil repeats:YES];
	
	// set up the timer to automatically start the recording
	autoStartTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(autoStart) userInfo:nil repeats:NO];
}

- (void)autoStart {
	// dismiss the alert view
	[autoStartAlertView dismissWithClickedButtonIndex:1 animated:YES];
	
	// shut off the other countdown timer
	[autoStartCountdownTimer invalidate];
	
	// start recording
	[self startNewRecording];
}

- (void)updateAutoStartCountdown {
	static int countdown = 9;
	
	// update the message in the auto start prompt
	[autoStartAlertView setMessage:[NSString stringWithFormat:@"The recording will automatically begin in %d seconds", countdown]];
	
	// decrease the countdown
	countdown--;
	
	// if we reach the bottom, shut off the timer 
	if (countdown == 0)
		[autoStartCountdownTimer invalidate];
}

#pragma mark - Button Presses 

- (void)thumbnailButtonPressed:(id)sender {
	// display the fvc
	[self.view bringSubviewToFront:fvc.view];
	[fvc setTitle:@"Loading..." andDescription:@""];
	[fvc startAnimating];
	
	dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		// get the clip to play from the button
		NSString *pathToClip = [(ThumbnailButton *)sender path];
	
		// initialize and display the clip player
		PlayerViewController *pvc = [[PlayerViewController alloc] initWithPathToClip:pathToClip];
		
		dispatch_async( dispatch_get_main_queue(), ^{
			// [self presentModalViewController:pvc animated:YES];
            [self presentViewController:pvc animated:YES completion:nil];
		});
	});
}

- (IBAction)recordButtonPressed:(id)sender {
	[self startNewRecording];
}

- (IBAction)foldersButtonPressed:(id)sender {
    FileViewerViewController *fvvc = [[FileViewerViewController alloc] init];
	// [self presentModalViewController:fvvc animated:YES];
    [self presentViewController:fvvc animated:YES completion:nil];
}

- (IBAction)settingsButtonPressed:(id)sender {
	SettingsViewController *svc = [[SettingsViewController alloc] init];
	// [self presentModalViewController:svc animated:YES];
    [self presentViewController:svc animated:YES completion:nil];
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Adjust UI if on 4 inch screen.
    if ([self is4InchScreen])
        [toolbar setCenter:CGPointMake(toolbar.center.x, toolbar.center.y + 88)];
    
	// variable initialization
	clipDisplayButtons = [[NSMutableArray alloc] init];
	
	// set up the fvc
	fvc = [[FeedbackViewController alloc] init];
	fvc.view.center = CGPointMake(160, 240);
	[self.view addSubview:fvc.view];
	
	// check if clips directory exists. If not, create it
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/"];
	NSString *clipsPath = [destinationPath stringByAppendingString:@"clips"];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:clipsPath]) {
		[fileManager createDirectoryAtPath:clipsPath withIntermediateDirectories:NO attributes:nil error:nil];
	}
	
	// check if screenshots directory exists. If not, create it
	paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/"];
	NSString *screenshotsPath = [destinationPath stringByAppendingString:@"screenshots"];
	if (![fileManager fileExistsAtPath:screenshotsPath]) {
		[fileManager createDirectoryAtPath:screenshotsPath withIntermediateDirectories:NO attributes:nil error:nil];
	}
    
    // check if location_logs directory exists. If not, create it
	paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/"];
	NSString *locationLogsPath = [destinationPath stringByAppendingString:@"location_logs"];
	if (![fileManager fileExistsAtPath:locationLogsPath]) {
		[fileManager createDirectoryAtPath:locationLogsPath withIntermediateDirectories:NO attributes:nil error:nil];
	}
    
	// initialize clip displays
	[self refreshClipDisplays];
    
    // Log the user in if there are credentials in NSUserDefaults.
    NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:DUICAM_USERNAME_KEY];
    if (email) {
        NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:DUICAM_PASSWORD_KEY];
        
        loginToDuicamOrg(email, password);
    }
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[fvc stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	// Only enter this function once.
	static BOOL firstRun = YES;
	if (!firstRun)
		return;
	firstRun = NO;
	
	// try to automatically start recording
	[self promptToAutoStart];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
