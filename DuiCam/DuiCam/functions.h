//
//  functions.h
//  DuiCam
//
//  Created by Daniel de Haas on 2/23/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#ifndef DuiCam_functions_h
#define DuiCam_functions_h

void loginToDuicamOrg(NSString *email, NSString *password);

#endif
