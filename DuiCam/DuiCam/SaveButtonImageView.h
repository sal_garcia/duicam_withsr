//
//  SaveButtonImageView.h
//  DuiCam
//
//  Created by Daniel de Haas on 11/3/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecorderViewController.h"

@class RecorderViewController;

@interface SaveButtonImageView : UIImageView 

@property RecorderViewController *touchDelegate;

@end
