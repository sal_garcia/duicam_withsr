//
//  DuicamOrgLoginViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/22/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import "DuicamOrgLoginViewController.h"

@interface DuicamOrgLoginViewController ()

@end

@implementation DuicamOrgLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    registerButton.layer.borderWidth = 0.5f;
    registerButton.layer.borderColor = [[UIColor colorWithRed:.7843 green:.7843 blue:.7843 alpha:1.0] CGColor];
    registerButton.layer.cornerRadius = 5;
    
    isRegisterMode = YES;
    
    [errorLabel setHidden:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(displayKeyboard) userInfo:nil repeats:NO];
    
    // Add tap handler to terms and conditions label.
    termsConditionsLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConditionsLabelTapped)];
    [termsConditionsLabel addGestureRecognizer:tapGesture];
}

- (void)termsAndConditionsLabelTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[DUICAM_ORG_URL stringByAppendingString:@"terms_and_conditions"]]];
}

- (void)displayKeyboard {
    [emailTextField becomeFirstResponder];
    
    [UIView animateWithDuration:1
                     animations:^{
                         // Get the height and width of the iPhone.
                         double screenHeight = [UIScreen mainScreen].bounds.size.height;
                         double screenWidth = [UIScreen mainScreen].bounds.size.width;
                         
                         // Set the height and content size of the scroll view.
                         CGRect scrollViewFrame = mainScrollView.frame;
                         double initialScrollViewHeight = scrollViewFrame.size.height;
                         mainScrollView.frame = (CGRect){ { scrollViewFrame.origin.x, scrollViewFrame.origin.y }, { screenWidth, screenHeight - KEYBOARD_HEIGHT - NAV_BAR_HEIGHT } };
                         mainScrollView.contentSize = (CGSize){ screenWidth , initialScrollViewHeight };
                     }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)errorWithTextField:(UITextField *)textField {
    textField.layer.borderColor = [[UIColor redColor] CGColor];
    textField.layer.borderWidth = 0.5f;
    textField.layer.cornerRadius = 5;
}

- (void)clearErrorOnTextField:(UITextField *)textField {
    textField.layer.borderColor = [[UIColor colorWithRed:.7843 green:.7843 blue:.7843 alpha:1.0] CGColor];
    textField.layer.borderWidth = 0.5f;
    textField.layer.cornerRadius = 5;
}

- (void)clearAllTextFields {
    // Verify all fields.
    NSArray *textFields = @[emailTextField, passwordTextField, confirmPasswordTextField, confirmationCodeTextField];
    
    // Clear errors from all fields.
    for (UITextField *next in textFields)
        [self clearErrorOnTextField:next];
}

- (IBAction)registerButtonPressed:(id)sender {
    if (isRegisterMode) {
        // Extract fields.
        NSString *email = emailTextField.text;
        NSString *password = passwordTextField.text;
        NSString *confirmPassword = confirmPasswordTextField.text;
        NSString *confirmationCode = confirmationCodeTextField.text;
        
        // Verify all fields.
        NSArray *textFields = @[emailTextField, passwordTextField, confirmPasswordTextField, confirmationCodeTextField];

        [self clearAllTextFields];

        // Check for blank fields.
        for (UITextField *next in textFields) {
            if ([next.text isEqualToString:@""]) {
                [self errorWithTextField:next];
                [next becomeFirstResponder];
                return;
            }
        }
        
        // Compare passwords.
        if (![password isEqualToString:confirmPassword]) {
            [self errorWithTextField:passwordTextField];
            [self errorWithTextField:confirmPasswordTextField];
            
            [passwordTextField becomeFirstResponder];
            
            return;
        }
        
        // Register the user.
        [self registerWithEmail:email andPassword:password andConfirmationCode:confirmationCode];
    } else {
        // Extract fields.
        NSString *email = emailTextField.text;
        NSString *password = passwordTextField.text;
        
        // Verify all fields.
        NSArray *textFields = @[emailTextField, passwordTextField];
        
        [self clearAllTextFields];
        
        // Check for blank fields.
        for (UITextField *next in textFields) {
            if ([next.text isEqualToString:@""]) {
                [self errorWithTextField:next];
                [next becomeFirstResponder];
                return;
            }
        }
        
        // Log the user in
        [self loginWithEmail:email andPassword:password];
    }
}

- (void)registerWithEmail:(NSString *)email andPassword:(NSString *)password andConfirmationCode:(NSString *)confirmationCode {
    // Get a CSRF token.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/xhtml+xml", @"text/html", nil];
    [manager GET:[DUICAM_ORG_URL stringByAppendingString:@"login/csrf"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *csrf = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             // Post to /json/register.
             AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
             manager.requestSerializer = [AFJSONRequestSerializer serializer];
             manager.responseSerializer = [AFJSONResponseSerializer serializer];
             NSDictionary *parameters = @{ @"csrf_token" : csrf,
                                           @"email" : email,
                                           @"password" : password,
                                           @"password_confirm" : password,
                                           @"confirmation_code" : confirmationCode,
                                           @"accept_terms" : [NSNumber numberWithBool:[termsConditionsSwitch isOn]] };
             [manager POST:[DUICAM_ORG_URL stringByAppendingString:@"json/register"]
                parameters:parameters
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       NSDictionary *response = (NSDictionary *)responseObject;
                       
                       // Check for success.
                       int successful = [[response objectForKey:@"success"] intValue];
                       NSLog(@"Success: %d", successful);
                       if (successful) {
                           // Store the user's username and password for later.
                           [[NSUserDefaults standardUserDefaults] setObject:email forKey:DUICAM_USERNAME_KEY];
                           [[NSUserDefaults standardUserDefaults] setObject:password forKey:DUICAM_PASSWORD_KEY];
                       } else {
                           [errorLabel setHidden:NO];
                           [errorLabel setText:[response objectForKey:@"msg"]];
                           
                           return;
                       }
                       
                       // Extract the session cookie and store it.
                       NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:DUICAM_ORG_URL]];
                       if ([cookies count] > 0) {
                           NSHTTPCookie *cookie = [cookies objectAtIndex:0];
                           NSMutableDictionary* cookieDictionary = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:DUICAM_KEYCHAIN_ITEM_NAME]];
                           [cookieDictionary setValue:cookie.properties forKey:DUICAM_ORG_URL];
                           [[NSUserDefaults standardUserDefaults] setObject:cookieDictionary forKey:DUICAM_KEYCHAIN_ITEM_NAME];
                       } else {
                           NSLog(@"No cookies after register");
                           return;
                       }
                       
                       if ([self.presentingViewController respondsToSelector:@selector(signinModalDismissed)]) {
                           [(SettingsViewController *)self.presentingViewController signinModalDismissed];
                       }
                       [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       NSLog(@"Register failure: %@", error);
                   }];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"CSRF token registration error: %@", error);
         }];
}

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password {
    // Get a CSRF token.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/xhtml+xml", @"text/html", nil];
    [manager GET:[DUICAM_ORG_URL stringByAppendingString:@"login/csrf"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *csrf = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             // Post to /json/login.
             AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
             manager.requestSerializer = [AFJSONRequestSerializer serializer];
             manager.responseSerializer = [AFJSONResponseSerializer serializer];
             NSDictionary *parameters = @{ @"csrf_token" : csrf,
                                           @"email" : email,
                                           @"password" : password };
             [manager POST:[DUICAM_ORG_URL stringByAppendingString:@"json/login"]
                parameters:parameters
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       // Extract results of the login operation.
                       NSDictionary *response = (NSDictionary*)responseObject;
                       NSString *message = [response objectForKey:@"msg"];
                       
                       // Check if login was successful or not.
                       if ([message isEqualToString:@"Welcome to the DuiCam site."]) {
                           // Hide the error label.
                           [errorLabel setHidden:YES];
                           
                           // Store the user's username and password for later.
                           [[NSUserDefaults standardUserDefaults] setObject:email forKey:DUICAM_USERNAME_KEY];
                           [[NSUserDefaults standardUserDefaults] setObject:password forKey:DUICAM_PASSWORD_KEY];
                       } else {
                           // Display error message.
                           [errorLabel setHidden:NO];
                           [errorLabel setText:@"Username or password is incorrect"];
                           
                           return;
                       }
                       
                       // Extract the session cookie and store it.
                       NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:DUICAM_ORG_URL]];
                       if ([cookies count] > 0) {
                           NSHTTPCookie *cookie = [cookies objectAtIndex:0];
                           NSMutableDictionary* cookieDictionary = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:DUICAM_KEYCHAIN_ITEM_NAME]];
                           [cookieDictionary setValue:cookie.properties forKey:DUICAM_ORG_URL];
                           [[NSUserDefaults standardUserDefaults] setObject:cookieDictionary forKey:DUICAM_KEYCHAIN_ITEM_NAME];
                       } else {
                           NSLog(@"No cookies after sign in");
                           return;
                       }
                       
                       if ([self.presentingViewController respondsToSelector:@selector(signinModalDismissed)]) {
                           [(SettingsViewController *)self.presentingViewController signinModalDismissed];
                       }
                       [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       NSLog(@"Register failure: %@", error);
                   }];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"CSRF token registration error: %@", error);
         }];
}

void loginToDuicamOrg(NSString *email, NSString *password) {
    // Get a CSRF token.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/xhtml+xml", @"text/html", nil];
    [manager GET:[DUICAM_ORG_URL stringByAppendingString:@"login/csrf"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *csrf = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             // Post to /json/login.
             AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
             manager.requestSerializer = [AFJSONRequestSerializer serializer];
             manager.responseSerializer = [AFJSONResponseSerializer serializer];
             NSDictionary *parameters = @{ @"csrf_token" : csrf,
                                           @"email" : email,
                                           @"password" : password };
             [manager POST:[DUICAM_ORG_URL stringByAppendingString:@"json/login"]
                parameters:parameters
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                       // Extract the session cookie and store it.
                       NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:DUICAM_ORG_URL]];
                       if ([cookies count] > 0) {
                           NSHTTPCookie *cookie = [cookies objectAtIndex:0];
                           NSMutableDictionary* cookieDictionary = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:DUICAM_KEYCHAIN_ITEM_NAME]];
                           [cookieDictionary setValue:cookie.properties forKey:DUICAM_ORG_URL];
                           [[NSUserDefaults standardUserDefaults] setObject:cookieDictionary forKey:DUICAM_KEYCHAIN_ITEM_NAME];
                       } else {
                           NSLog(@"No cookies after sign in");
                           return;
                       }
                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       NSLog(@"Register failure: %@", error);
                   }];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"CSRF token registration error: %@", error);
         }];
}

- (IBAction)alreadyHaveAccountButtonPressed:(id)sender {
    [errorLabel setHidden:YES];
    
    const double registerButtonOffset = 100;
    
    isRegisterMode = !isRegisterMode;
    [self clearAllTextFields];
    if (isRegisterMode) {
        [UIView animateWithDuration:0.1
                         animations:^{
                             confirmPasswordLabel.alpha = 1.0;
                             confirmPasswordTextField.alpha = 1.0;
                             confirmationCodeLabel.alpha = 1.0;
                             confirmationCodeTextField.alpha = 1.0;
                             termsConditionsLabel.alpha = 1.0;
                             termsConditionsSwitch.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             CGPoint regButtonCenter = registerButton.center;
                             registerButton.center = CGPointMake(regButtonCenter.x, regButtonCenter.y + registerButtonOffset);
                             CGPoint alreadyHaveAccountCenter = alreadyHaveAccountButton.center;
                             alreadyHaveAccountButton.center = CGPointMake(alreadyHaveAccountCenter.x, alreadyHaveAccountCenter.y + registerButtonOffset);
                         }];
        [registerButton setTitle:@"Register" forState:UIControlStateNormal];
        [alreadyHaveAccountButton setTitle:@"Already have an account?" forState:UIControlStateNormal];
    } else {
        [UIView animateWithDuration:0.1
                         animations:^{
                             confirmPasswordLabel.alpha = 0.0;
                             confirmPasswordTextField.alpha = 0.0;
                             confirmationCodeLabel.alpha = 0.0;
                             confirmationCodeTextField.alpha = 0.0;
                             termsConditionsLabel.alpha = 0.0;
                             termsConditionsSwitch.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             CGPoint regButtonCenter = registerButton.center;
                             registerButton.center = CGPointMake(regButtonCenter.x, regButtonCenter.y - registerButtonOffset);
                             CGPoint alreadyHaveAccountCenter = alreadyHaveAccountButton.center;
                             alreadyHaveAccountButton.center = CGPointMake(alreadyHaveAccountCenter.x, alreadyHaveAccountCenter.y - registerButtonOffset);
                         }];
        [registerButton setTitle:@"Sign in" forState:UIControlStateNormal];
        [alreadyHaveAccountButton setTitle:@"Don't have an account?" forState:UIControlStateNormal];
    }
}

@end
