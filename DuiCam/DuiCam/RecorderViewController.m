//
//  RecorderViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "RecorderViewController.h"

@interface RecorderViewController () {
	int firstVisitCount;
    
    BOOL saveAllClips;
}

@end

@implementation RecorderViewController

#pragma mark - AVCapture Delegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
	// Check if there was an error with the recording
	BOOL recordedSuccessfully = YES;
    if ([error code] != noErr) {
        // A problem occurred: Find out if the recording was successful.
        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (value) {
            recordedSuccessfully = [value boolValue];
        }
		if (!recordedSuccessfully)
			[[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Recording unsuccessful: %@", [value description]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
		return;
    }
	
	// store the end date
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[@"kClipEnd" stringByAppendingString:[outputFileURL path]]];
	
	// notify the ViewController that a new clip is available
	[self.delegate newClipSavedAtPath:[outputFileURL path]];
	
    // If user has turned on "Save all" feature, save the most recent clip.
    if (saveAllClips)
        [self saveClipAtPath:[outputFileURL path]];
    
	// check if the clip length timer fired and requested a new recording
	if (shouldStartNewRecording) {
        if (shouldUploadClip) {
            shouldUploadClip = NO;
            
            NSLog(@"doing stuff here");
            
            // Instantiate PVC with path to clip.
            PlayerViewController *pvc = [[PlayerViewController alloc] initWithPathToClip:[outputFileURL path]];
            
            // Generate filename and description.
            NSString *filename = @"This is a test filename";
            NSString *description = @"This is a test description";
            NSArray *tags = @[@"DuiCam"];
            
            // Call upload on PVC.
            [pvc uploadVideoWithTitle:filename andDescription:description andTags:tags andShare:NO];
        }
        
		shouldStartNewRecording = NO;
		
		[self startRecording];
	} else {
		// display a check in the fvc
		[fvc setTitle:@""];
		[fvc checkAndFade];
		
		// dismiss this view
		// [self dismissModalViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
	}
}

#pragma mark - AV Capture

- (void)setupAVCapture {
	// Create and configure the AVCaptureSession
	session = [AVCaptureSession new];
	NSString *newPreset = [[NSUserDefaults standardUserDefaults] objectForKey:@"kLastUsedPresetKey"];
	if (!newPreset) {
		newPreset = AVCaptureSessionPresetHigh;
		[[NSUserDefaults standardUserDefaults] setObject:AVCaptureSessionPresetHigh forKey:@"kLastUsedPresetKey"];
	}
	if ([session canSetSessionPreset:newPreset])
		[session setSessionPreset:newPreset];
	else
		[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Error setting session preset" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	
	// Set up AVCaptureSession inputs
	NSError *error = nil;
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if ([session canAddInput:deviceInput])
		[session addInput:deviceInput];
	else
		[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Error adding device input" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	
	// Set up the movie file output
	movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
	if ([session canAddOutput:movieFileOutput])
		[session addOutput:movieFileOutput];
	else
		[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Error adding movie file output" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	
	// Set up the preview layer
	previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
	[previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
	[previewLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
	CALayer *rootLayer = cameraView.layer;
	[rootLayer setMasksToBounds:YES];
	[previewLayer setFrame:[rootLayer bounds]];
	[rootLayer addSublayer:previewLayer];
}

#pragma mark - UI Control

- (void)showRecordingControls {
	[cameraView bringSubviewToFront:controlsContainerView];
}

#pragma mark - Dimmer methods

- (void)toggleDimmer {
	// alternate between hidden and not hidden
	BOOL dimmerHidden = dimmerButton.hidden;
	[dimmerButton setHidden:!dimmerHidden];
	[dimmerLabel setHidden:!dimmerHidden];
	
	// bring the dimmer view/label to the front
	[cameraView bringSubviewToFront:dimmerButton];
	[cameraView bringSubviewToFront:dimmerLabel];
	
	// put the recording controls on top of everything
	[self showRecordingControls];
}

- (void)turnOnDimmer {
	[dimmerButton setHidden:NO];
	[dimmerLabel setHidden:NO];
	
	// bring the dimmer view/label to the front
	[cameraView bringSubviewToFront:dimmerButton];
	[cameraView bringSubviewToFront:dimmerLabel];
	
	// put the recording controls on top of everything
	[self showRecordingControls];
}

#pragma mark - Timestamp methods 

- (void)updateTimestamp {
	// get the current date
	NSDate *currentDate = [NSDate date];
	
	// get the time difference in seconds
	int secondsDiff = [currentDate timeIntervalSinceDate:recordingStartDate];
	
	// convert to hours/minutes/seconds
	int hours = secondsDiff / (60 * 60);
	secondsDiff = secondsDiff % (60 * 60);
	int minutes = secondsDiff / 60;
	secondsDiff = secondsDiff % 60;
	
	// display the time in the label
	timestampLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, secondsDiff];
}

#pragma mark - Clip Saving

- (void)saveClipAtPath:(NSString *)path {
	// get list of saved clips
	NSArray *savedClips = [[NSUserDefaults standardUserDefaults] stringArrayForKey:@"kSavedClips"];
	if (!savedClips)
		savedClips = [[NSArray alloc] init];
	
	// check if this clip is already saved
	// if so, just return
	if ([savedClips indexOfObject:path] != NSNotFound)
		return;
	
	// build a mutable array to add the new path to
	NSMutableArray *newSavedClips = [[NSMutableArray alloc] initWithArray:savedClips];
	
	// add the path of the current video to the end
	[newSavedClips addObject:path];
	
	// put the array back into user defaults
	[[NSUserDefaults standardUserDefaults] setObject:newSavedClips forKey:@"kSavedClips"];
}

- (void)quickSave {
    // display fvc
	[fvc setTitle:@"Saved" andDescription:@"Clip saved to folders"];
	[fvc checkAndFade];
	
	// get array of clips ordered by creation date
	NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
	NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
	for (NSDictionary *next in orderedClipsDict)
		[orderedClips addObject:[next objectForKey:@"path"]];
	
	
	// if there are at least 2 clips, save the last 2 (which includes the current one).
	// otherwise, just save this clip.
	if ([orderedClips count] >= 2) {
		[self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 2]];
		[self saveClipAtPath:[orderedClips lastObject]];
	} else {
		[self saveClipAtPath:[orderedClips lastObject]];
	}
    
	// trigger a clip switch so we can be sure this clip will be saved
	[self triggerClipSwitch];
}

- (void)longSave {
    // display fvc
	[fvc setTitle:@"Saved" andDescription:@"Clip saved to folders"];
	[fvc checkAndFade];
	
	// get array of clips ordered by creation date
	NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
	NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
	for (NSDictionary *next in orderedClipsDict)
		[orderedClips addObject:[next objectForKey:@"path"]];
	
	
	// save up to the last 3 clips (which includes the current one)
    if ([orderedClips count] >= 3) {
        [self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 3]];
        [self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 2]];
		[self saveClipAtPath:[orderedClips lastObject]];
    } else if ([orderedClips count] >= 2) {
		[self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 2]];
		[self saveClipAtPath:[orderedClips lastObject]];
	} else {
		[self saveClipAtPath:[orderedClips lastObject]];
	}
    
	// trigger a clip switch so we can be sure this clip will be saved
	[self triggerClipSwitch];
}

#pragma mark - Button Presses 

- (IBAction)stopButtonPressed:(id)sender {
	// show the feedback view
	[fvc setTitle:@"Stopping..." andDescription:@""];
	[fvc startAnimating];
	
	dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		// stop the clip length timer from firing again
		[clipLengthTimer invalidate];
		
		// make sure a new clip won't start recording after the last one saves
		shouldStartNewRecording = NO;
		
		// stop the current recording
		[self stopRecording];
		
		// stop the session to freeze the view
		[session stopRunning];
	});
}

- (IBAction)brightnessButtonPressed:(id)sender {
	[self toggleDimmer];
}

- (IBAction)saveButtonPressed:(id)sender {
	// display fvc
	[fvc setTitle:@"Saved" andDescription:@"Clip saved to folders"];
	[fvc checkAndFade];
	
	// save previous and current clips
	// get array of clips ordered by creation date
	NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
	NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
	for (NSDictionary *next in orderedClipsDict)
		[orderedClips addObject:[next objectForKey:@"path"]];
	
	
	// if there are at least 2 clips, save the last 2 (which includes the current one).
	// otherwise, just save this clip.
	if ([orderedClips count] >= 2) {
		[self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 2]];
		[self saveClipAtPath:[orderedClips lastObject]];
	} else {
		[self saveClipAtPath:[orderedClips lastObject]];
	}

	// trigger a clip switch so we can be sure this clip will be saved
	[self triggerClipSwitch];
}

- (IBAction)dimmerButtonPressed:(id)sender {
	[self toggleDimmer];
}

- (IBAction)camcorderButtonPressed:(id)sender {
    if (!saveAllClips) {
        saveAllClips = YES;
        
        // Change the buttons image to blank.
        [sender setImage:[UIImage imageNamed:@"blank.png"] forState:UIControlStateNormal];
        
        // Start camcorder activity indicator.
        [camcorderActivityIndicator startAnimating];
        
        // Show warning label.
        [savingAllClipsLabel setHidden:NO];
        [savingAllClipsLabel setAlpha:1.0];
        [self animateCamcorderWarningLabel];
        camcorderWarningLabelAnimationEnabled = YES;
        [self.view bringSubviewToFront:savingAllClipsLabel];
    } else {
        saveAllClips = NO;
        
        // Save current clip.
        // get array of clips ordered by creation date
        NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
        NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
        for (NSDictionary *next in orderedClipsDict)
            [orderedClips addObject:[next objectForKey:@"path"]];
        [self saveClipAtPath:[orderedClips lastObject]];

        // Change the buttons image to the normal image.
        [sender setImage:[UIImage imageNamed:@"camcorder.png"] forState:UIControlStateNormal];

        // Stop camcorder activity indicator.
        [camcorderActivityIndicator stopAnimating];
        
        // Show warning label.
        [savingAllClipsLabel setHidden:YES];
        camcorderWarningLabelAnimationEnabled = NO;
        
        // Trigger clip switch.
        [self triggerClipSwitch];
    }
}

- (void)hideLongSaveButtonImageView {
    longSaveButtonImageView.hidden = YES;
    uploadButtonImageView.hidden = YES;
}

- (void)quickSaveButtonPressed:(UIGestureRecognizer *)recognizer {
    // display fvc
	[fvc setTitle:@"Saved" andDescription:@"Clip saved to folders"];
	[fvc checkAndFade];
	
	// save previous and current clips
	// get array of clips ordered by creation date
	NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
	NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
	for (NSDictionary *next in orderedClipsDict)
		[orderedClips addObject:[next objectForKey:@"path"]];
	
	
	// if there are at least 2 clips, save the last 2 (which includes the current one).
	// otherwise, just save this clip.
	if ([orderedClips count] >= 2) {
		[self saveClipAtPath:[orderedClips objectAtIndex:[orderedClips count] - 2]];
		[self saveClipAtPath:[orderedClips lastObject]];
	} else {
		[self saveClipAtPath:[orderedClips lastObject]];
	}
    
	// trigger a clip switch so we can be sure this clip will be saved
	[self triggerClipSwitch];
}

- (void)quickSaveButtonTouchDown:(UIGestureRecognizer *)recognizer {
    longSaveButtonImageView.hidden = NO;
    uploadButtonImageView.hidden = NO;
}

#pragma mark - Camcorder animation

- (void)animateCamcorderWarningLabel {
    [UIView animateWithDuration:1.5
                     animations:^{
                         [savingAllClipsLabel setAlpha:0.5];
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:1.5
                                          animations:^{
                                              [savingAllClipsLabel setAlpha:1.0];
                                          }
                                          completion:^(BOOL finished) {
                                              if (camcorderWarningLabelAnimationEnabled)
                                                  [self animateCamcorderWarningLabel];
                                          }];
                     }];
}

#pragma mark - File naming

- (NSString *)generateFilenameForCurrentTimeForClip:(BOOL)isClip {
	// generate the file name
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MM-dd-YY_HH:mm:ss"];
	NSString *fileName = [formatter stringFromDate:[NSDate date]];
    if (isClip) {
        fileName = [fileName stringByAppendingString:@".mp4"];
    } else {
        fileName = [fileName stringByAppendingString:@".txt"];
    }
	
	// append the file name to the documents directory file name
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *destinationPath;
    if (isClip) {
        destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/clips/"];
    } else {
        destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/location_logs/"];
    }
	
	destinationPath = [destinationPath stringByAppendingString:fileName];
	
	return destinationPath;
}

#pragma mark - Clip Deletion

- (void)deleteOldestClip {
	// get array of clips ordered by creation date
	NSArray *orderedClipsDict = [self getOrderedClipFilepaths];
	NSMutableArray *orderedClips = [[NSMutableArray alloc] init];
	for (NSDictionary *next in orderedClipsDict)
		[orderedClips addObject:[next objectForKey:@"path"]];

	// get list of saved clips
	NSArray *savedClips = [[NSUserDefaults standardUserDefaults] stringArrayForKey:@"kSavedClips"];
	if (!savedClips)
		savedClips = [[NSArray alloc] init];
	
	// filter out saved clips
	NSMutableArray *nonSavedClips = [[NSMutableArray alloc] init];
	for (NSString *filename in orderedClips) {
		if ([savedClips indexOfObject:filename] == NSNotFound)
			[nonSavedClips addObject:filename];
	}
	
	// check if any clips should be deleted, and delete them
	NSFileManager *fileManager = [NSFileManager defaultManager];
	for (int i = 0; i < [nonSavedClips count] - 2 && [nonSavedClips count] > 2; ++i) {
		NSString *path = [nonSavedClips objectAtIndex:i];
		// delete the file
		[fileManager removeItemAtPath:path error:nil];
        
        // Delete the associated location log file.
        NSString *locationLogPath = [[[path substringToIndex:([path length] - 3)] stringByAppendingString:@"txt"] stringByReplacingOccurrencesOfString:@"/clips/" withString:@"/location_logs/"];
        [fileManager removeItemAtPath:locationLogPath error:nil];
        
		// remove the begin and end dates from User Defaults
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:[@"kClipBegin" stringByAppendingString:path]];
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:[@"kClipEnd" stringByAppendingString:path]];
	}
}

- (NSArray *)getOrderedClipFilepaths {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *destinationPath = [[paths objectAtIndex:0] stringByAppendingString:@"/clips/"];
	
	NSError* error = nil;
	NSArray* filesArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error];
	if(error != nil) {
		NSLog(@"Error in reading files: %@", [error localizedDescription]);
		return [NSArray array];
	}
	
	// sort by creation date
	NSMutableArray* filesAndProperties = [NSMutableArray arrayWithCapacity:[filesArray count]];
	for(NSString* file in filesArray) {
		NSString* filePath = [destinationPath stringByAppendingPathComponent:file];
		NSDictionary* properties = [[NSFileManager defaultManager]
									attributesOfItemAtPath:filePath
									error:&error];
		NSDate* modDate = [properties objectForKey:NSFileModificationDate];
		
		if(error == nil)
		{
			[filesAndProperties addObject:[NSDictionary dictionaryWithObjectsAndKeys:
										   filePath, @"path",
										   modDate, @"lastModDate",
										   nil]];
		}
	}
	
    // sort using a block
    // order inverted as we want latest date first
	NSArray* sortedFiles = [filesAndProperties sortedArrayUsingComparator:
							^(id path1, id path2)
							{
								// compare
								NSComparisonResult comp = [[path1 objectForKey:@"lastModDate"] compare:
														   [path2 objectForKey:@"lastModDate"]];
								return comp;
							}];
	
	return sortedFiles;
}


#pragma mark - Recording Control

- (void)startRecording {
	// delete old clips
	[self deleteOldestClip];

	// generate filename for current time
	pathToRecording = [self generateFilenameForCurrentTimeForClip:YES];
    pathToLocationLog = [self generateFilenameForCurrentTimeForClip:NO];
    
    // Create the location log file.
    char *initString = "---\n";
    NSData *data = [[NSData alloc] initWithBytes:initString length:4];
    [data writeToFile:pathToLocationLog atomically:YES];

	// if a file already exists where we're trying to record, the recording will fail
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:pathToRecording])
		[fileManager removeItemAtPath:pathToRecording error:nil];

	// make sure UI shows up
	[self showRecordingControls];

	// store the begin date for this clip
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[@"kClipBegin" stringByAppendingString:pathToRecording]];
	
	// start the clip length timer
	// get clip length from User Defaults
	int clipLength = [[NSUserDefaults standardUserDefaults] integerForKey:@"kClipLength"];
	if (clipLength == 0) {
		clipLength = 3 * 60; // default is 3 minute clips
		[[NSUserDefaults standardUserDefaults] setInteger:clipLength forKey:@"kClipLength"];
	}
	clipLengthTimer = [NSTimer scheduledTimerWithTimeInterval:clipLength target:self selector:@selector(clipLengthTimerFired) userInfo:nil repeats:NO];

	// start the capture session and the recording
	[session startRunning];
	NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:pathToRecording];
	[movieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
}

- (void)stopRecording {
	// stop recording to the output file
	[movieFileOutput stopRecording];
    
    [locationManager stopUpdatingLocation];
}

- (void)triggerClipSwitch {
	// invalidate the clip switch timer in case it's still running
	[clipLengthTimer invalidate];
	
	// set flag to start a new recording
	shouldStartNewRecording = YES;
	
	// stop current recording
	[self stopRecording];
}

- (void)clipLengthTimerFired {
	[self triggerClipSwitch];
}

#pragma mark - Location services 

- (void)initLocationServices {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    pastLocations = [[NSMutableArray alloc] init];
    
    [locationManager startUpdatingLocation];
    
    locationTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getNewLocation) userInfo:nil repeats:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    // Write the current location to a text file.
    CLLocation *currentLocation = [locations lastObject];
    
    NSString *logLine = [NSString stringWithFormat:@"%@,%f,%f,%f MPH\n", [currentLocation.timestamp description], currentLocation.coordinate.latitude, currentLocation.coordinate.longitude, currentLocation.speed];
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:pathToLocationLog];
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[logLine dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandler closeFile];
    
    [pastLocations addObjectsFromArray:locations];
    
    [self updateSpeedLabel];
    
    [locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
    [locationManager setDistanceFilter:99999];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location updates failed: %@", error);
}

- (void)updateSpeedLabel {
    if ([pastLocations count] == 0) {
        [speedLabel setText:@"0 MPH"];
        return;
    }
    
    // Get most recent location.
    CLLocation *lastLocation = [pastLocations lastObject];
    
    double speed = lastLocation.speed;
    if (speed == -1)
        speed = 0;
    
    // Convert from m/s to MPH.
    speed = speed * MILES_PER_METER * SECONDS_PER_HOUR;

    // Update speed label to milesPerHour.
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithFloat:speed]];
    [speedLabel setText:[formatted stringByAppendingString:@" MPH"]];
}

- (void)getNewLocation {
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
}

- (BOOL)is4InchScreen {
    return [[UIScreen mainScreen] bounds].size.height == 568;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // If on a 4 inch screen, make the camera view larger.
    if ([self is4InchScreen]) {
        [cameraView setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
        [controlsContainerView setFrame:(CGRect){ { controlsContainerView.frame.origin.x, controlsContainerView.frame.origin.y + 88 }, { controlsContainerView.frame.size.width, controlsContainerView.frame.size.height } }];
        [dimmerButton setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    }
	
	[self setupAVCapture];
	
	// variable initialization
	// don't start a new recording unless the clip timer fires
	shouldStartNewRecording = NO;
    saveAllClips = NO;
    camcorderWarningLabelAnimationEnabled = NO;
	
	// set up hack for the autorotation
	firstVisitCount = 0;
	
	// set up the feedback view
	fvc = [[FeedbackViewController alloc] init];
	fvc.view.center = CGPointMake(160, 240);
	[cameraView addSubview:fvc.view];
	
	// set up timestamp date and timer
	recordingStartDate = [NSDate date];
	timestampUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimestamp) userInfo:nil repeats:YES];
    
    // Give the save buttons delegates.
    quickSaveButtonImageView.touchDelegate = self;
    
    // Flag controlling whether the current clip should be uploaded after the capture completes.
    shouldUploadClip = NO;
    
    // Set up location monitoring.
    [self initLocationServices];
    
	[self startRecording];
    
    // Turn on the dimmer after a bit of time.
    [NSTimer scheduledTimerWithTimeInterval:DIMMER_DELAY target:self selector:@selector(turnOnDimmer) userInfo:nil repeats:NO];
    
    // Enable battery monitoring.
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(batteryStateDidChange:)
                                                 name:UIDeviceBatteryStateDidChangeNotification
                                               object:nil];
    lastBatteryNotification = 0;
    lastState = [UIDevice currentDevice].batteryState;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];
    
    cameraView = nil;
	controlsContainerView = nil;
	dimmerButton = nil;
	dimmerLabel = nil;
	timestampLabel = nil;
	[timestampUpdateTimer invalidate];
	timestampUpdateTimer = nil;
    [super viewDidUnload];
}

#pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == METADATA_ALERT_VIEW) {
        // 0 is cancel, 1 is trim
        if (buttonIndex == 1) {
            // Set upload flag.
            shouldUploadClip = YES;
            
            // Stop recording.
            [self triggerClipSwitch];
        } else {
            NSLog(@"Edit metadata");
        }
    } else if (alertView.tag == BATTERY_ALERT_VIEW) {
        // 0 is cancel, 1 is trim
        if (buttonIndex == 1) {
            [self stopButtonPressed:nil];
            [cancelRecordingTimer invalidate];
            [alertView dismissWithClickedButtonIndex:1 animated:NO];
        } else {
            [cancelRecordingTimer invalidate];
            [alertView dismissWithClickedButtonIndex:1 animated:NO];
        }
    }
}

# pragma mark - Battery state monitoring

- (void)batteryStateDidChange:(NSNotification *)notification {
    UIDeviceBatteryState currentState = [UIDevice currentDevice].batteryState;
    long currentTime = [[NSDate date] timeIntervalSince1970];
    if (currentTime - lastBatteryNotification < 5 || lastState == currentState) {
        return;
    }
    lastBatteryNotification = currentTime;
    lastState = currentState;
    if (currentState == UIDeviceBatteryStateUnplugged) {
        // Start a timer to cancel recording.
        cancelRecordingTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(cancelTimerTick) userInfo:nil repeats:YES];
        cancelTimerCount = 10;
        
        // Show alert with countdown.
        cancelAlertView = [[UIAlertView alloc] initWithTitle:@"Device unplugged" message:[NSString stringWithFormat:@"Recording will automatically stop in %d seconds", cancelTimerCount] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Stop now", nil];
        cancelAlertView.tag = BATTERY_ALERT_VIEW;
        [cancelAlertView show];
    }
}

- (void)cancelTimerTick {
    cancelTimerCount -= 1;
    
    // Update the alert view message.
    cancelAlertView.message = [NSString stringWithFormat:@"Recording will automatically stop in %d seconds", cancelTimerCount];
    
    if (cancelTimerCount == 0) {
        [cancelAlertView dismissWithClickedButtonIndex:0 animated:NO];
        [self stopButtonPressed:nil];
        [cancelRecordingTimer invalidate];
    }
}

// SaveButtonImageView delegate methods.
#pragma mark - SaveButtonImageView delegate methods.

- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesBeganAtPoint:(UITouch *)touch {
    // Display the long save image view.
    longSaveButtonImageView.hidden = NO;
    longSaveButtonImageView.alpha = 1.0;
//    uploadButtonImageView.hidden = NO;
//    uploadButtonImageView.alpha = 1.0;
    
    // Darken the quick save image view as if pressed.
    quickSaveButtonImageView.alpha = 0.6;
}

- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesMovedAtPoint:(UITouch *)touch {
    // Convert touch point to save button view coordinates.
    CGPoint pointInImageView = [touch locationInView:quickSaveButtonImageView];
    
    if (CGRectContainsPoint(CGRectMake(0, 0, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
        longSaveButtonImageView.alpha = 1.0;
        quickSaveButtonImageView.alpha = 0.6;
//        uploadButtonImageView.alpha = 1.0;
    } else if (CGRectContainsPoint(CGRectMake(0, -1 * quickSaveButtonImageView.frame.size.height, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
        longSaveButtonImageView.alpha = 0.6;
        quickSaveButtonImageView.alpha = 1.0;
//        uploadButtonImageView.alpha = 1.0;
//    } else if (CGRectContainsPoint(CGRectMake(0, -2 * quickSaveButtonImageView.frame.size.height, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
////        uploadButtonImageView.alpha = 0.6;
//        longSaveButtonImageView.alpha = 1.0;
//        quickSaveButtonImageView.alpha = 1.0;
    } else {
        longSaveButtonImageView.alpha = 1.0;
        quickSaveButtonImageView.alpha = 1.0;
//        uploadButtonImageView.alpha = 1.0;
    }
}

- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesEndedAtPoint:(UITouch *)touch {
    // Convert touch point to save button view coordinates.
    CGPoint pointInImageView = [touch locationInView:quickSaveButtonImageView];
    
    // Hide the long save image view.
    longSaveButtonImageView.hidden = YES;
    quickSaveButtonImageView.alpha = 1.0;
    uploadButtonImageView.hidden = YES;
    
    // Determine which button's functionality to invoke.
    if (CGRectContainsPoint(CGRectMake(0, 0, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
        [self quickSave];
    } else if (CGRectContainsPoint(CGRectMake(0, -1 * quickSaveButtonImageView.frame.size.height, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
        [self longSave];
//    } else if (CGRectContainsPoint(CGRectMake(0, -2 * quickSaveButtonImageView.frame.size.height, quickSaveButtonImageView.frame.size.width, quickSaveButtonImageView.frame.size.height), pointInImageView)) {
//        UIAlertView *uploadAlertView = [[UIAlertView alloc] initWithTitle:@"Upload video" message:@"Would you like to edit the video metadata?" delegate:self cancelButtonTitle:@"Edit metadata" otherButtonTitles:@"Just upload", nil];
//        uploadAlertView.tag = METADATA_ALERT_VIEW;
//        [uploadAlertView show];
    }
}

- (void)saveButtonImageView:(SaveButtonImageView *)sbiv touchesCancelledAtPoint:(UITouch *)touch {
    longSaveButtonImageView.hidden = YES;
    quickSaveButtonImageView.alpha = 1.0;
    uploadButtonImageView.hidden = YES;
}

#pragma mark - Interface Orientations

- (void)rotateRecordingControlsToAngle:(double)angle {
	for (UIView *next in [controlsContainerView subviews]) {
		// don't rotate the timestamp label here 
		if ([next class] == [UILabel class])
			continue;
		[next setTransform:CGAffineTransformMakeRotation(angle)];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	if (toInterfaceOrientation == UIDeviceOrientationPortrait) {
		[self rotateRecordingControlsToAngle:0];
	} else if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft) {
		[self rotateRecordingControlsToAngle:(M_PI / 2)];
	} else if (toInterfaceOrientation == UIDeviceOrientationLandscapeRight) {
		[self rotateRecordingControlsToAngle:(-M_PI / 2)];
	}
	return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations {
	// hack to avoid weird shaped buttons if the recorder opens up in landscape mode 
	if (firstVisitCount < 2) {
		firstVisitCount++;
		return UIInterfaceOrientationMaskPortrait;
	}
	
	UIDeviceOrientation current = [[UIDevice currentDevice] orientation];
	double angle = 0;
	if (current == UIDeviceOrientationPortrait)
		angle = 0;
	else if (current == UIDeviceOrientationLandscapeLeft)
		angle = M_PI / 2;
	else if (current == UIDeviceOrientationLandscapeRight)
		angle = -M_PI / 2;
	
	// rotate controls and dimmer label
	[UIView animateWithDuration:0.5
					 animations:^{
						 // rotate the buttons
						 [self rotateRecordingControlsToAngle:angle];
						 
						 // rotate and translate the timestamp label
                         if (angle == 0) {
                             timestampLabel.layer.transform = CATransform3DMakeTranslation(0, 0, 0);
                             speedLabel.layer.transform = CATransform3DMakeTranslation(0, 0, 0);
                         } else {
                             timestampLabel.layer.transform = CATransform3DMakeTranslation(79, -38, 0);
                             speedLabel.layer.transform = CATransform3DMakeTranslation(55, -7, 0);
                         }
						 timestampLabel.layer.transform = CATransform3DRotate(timestampLabel.layer.transform, angle, 0, 0, 1);
                         speedLabel.layer.transform = CATransform3DRotate(speedLabel.layer.transform, angle, 0, 0, 1);
							 
						 // rotate the dimmer label
						 dimmerLabel.layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);
					 } completion:^(BOOL finished) {
						 dimmerLabel.center = CGPointMake(160.0, 153.0);
					 }];
	dimmerLabel.center = CGPointMake(160.0, 153.0);
	
	return UIInterfaceOrientationMaskPortrait;
}

@end
