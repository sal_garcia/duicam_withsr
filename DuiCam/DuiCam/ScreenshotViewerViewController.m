//
//  ScreenshotViewerViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "ScreenshotViewerViewController.h"

@interface ScreenshotViewerViewController () {
	UIImage *image;
	UIImageView *imageView;
}

@end

@implementation ScreenshotViewerViewController

#pragma mark - Buttons

- (IBAction)shareButtonPressed:(id)sender {
	UIActionSheet *shareOptions = [[UIActionSheet alloc] initWithTitle:@"Share screenshot" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"Save to photo roll", nil];
	[shareOptions showFromToolbar:bottomBar];
}

- (IBAction)doneButtonPressed:(id)sender {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)plusButtonPressed:(id)sender {
	// hide the UI
	[self hideTopAndBottomBar];
	
	double currentZoomLevel = [mainScrollView zoomScale];
	double newZoomLevel = currentZoomLevel + 1;
	if (newZoomLevel > [mainScrollView maximumZoomScale]) {
		newZoomLevel = [mainScrollView maximumZoomScale];
	}
	[mainScrollView setZoomScale:newZoomLevel animated:YES];
}

- (IBAction)minusButtonPressed:(id)sender {
	// hide the UI
	[self hideTopAndBottomBar];
	
	double currentZoomLevel = [mainScrollView zoomScale];
	double newZoomLevel = currentZoomLevel - 1;
	if (newZoomLevel < [mainScrollView minimumZoomScale]) {
		newZoomLevel = [mainScrollView minimumZoomScale];
	}
	[mainScrollView setZoomScale:newZoomLevel animated:YES];
}

#pragma mark - Gesture handlers

- (void)scrollViewTapped {
	[self toggleTopAndBottomBar];
}

#pragma mark - UI showing/hiding

- (void)hideTopAndBottomBar {
	[UIView animateWithDuration:0.3
					 animations:^{
						 topBar.alpha = 0.0;
						 bottomBar.alpha = 0.0;
					 }];
}

- (void)showTopAndBottomBar {
	[UIView animateWithDuration:0.3
					 animations:^{
						 topBar.alpha = 1.0;
						 bottomBar.alpha = 1.0;
					 }];
}

- (void)toggleTopAndBottomBar {
	if (topBar.alpha < 0.5)
		[self showTopAndBottomBar];
	else
		[self hideTopAndBottomBar];
}

#pragma mark - mail composer delegate 

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0:
		{
			// email
			if (![MFMailComposeViewController canSendMail]) {
				[[[UIAlertView alloc] initWithTitle:@"Error" message:@"This device is not currently set up to send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
				return;
			}
			MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
			mailComposer.mailComposeDelegate = self;
			[mailComposer addAttachmentData:UIImagePNGRepresentation(image) mimeType:@"image/png" fileName:@"screenshot.png"];
			// [self presentModalViewController:mailComposer animated:YES];
            [self presentViewController:mailComposer animated:YES completion:nil];
		}
			break;
		case 1:
		{
			// save
			UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
		}
			break;
		default:
			break;
	}
}

#pragma mark - Scroll view delegate 

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return imageView;
}

#pragma mark - View lifecycle 

- (id)initWithImage:(UIImage *)_image {
	self = [super init];
	if (self) {
		image = _image;
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// add the image to the scroll view
	imageView = [[UIImageView alloc] initWithImage:image];
	imageView.frame = mainScrollView.bounds;
	[mainScrollView addSubview:imageView];
	
	// add ourselves as a delegate of the scroll view to all for zooming
	mainScrollView.delegate = self;
	
	// set up a tap recognizer
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped)];
	[imageView addGestureRecognizer:tapGesture];
	imageView.userInteractionEnabled = YES;
	
	mainScrollView.canCancelContentTouches = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	mainScrollView = nil;
	topBar = nil;
	bottomBar = nil;
	[super viewDidUnload];
}
@end
