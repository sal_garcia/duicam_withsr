//
//  YoutubeUploadViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/20/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import "YoutubeUploadViewController.h"

@interface YoutubeUploadViewController ()

@end

@implementation YoutubeUploadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    descriptionTextView.layer.borderWidth = 0.5f;
    descriptionTextView.layer.borderColor = [[UIColor colorWithRed:.7843 green:.7843 blue:.7843 alpha:1.0] CGColor];
    descriptionTextView.layer.cornerRadius = 5;
    
    uploadButton.layer.borderWidth = 0.5f;
    uploadButton.layer.borderColor = [[UIColor colorWithRed:.7843 green:.7843 blue:.7843 alpha:1.0] CGColor];
    uploadButton.layer.cornerRadius = 5;
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(displayKeyboard) userInfo:nil repeats:NO];
}

- (void)displayKeyboard {
    [titleTextField becomeFirstResponder];
    
    [UIView animateWithDuration:1
                     animations:^{
                         // Get the height and width of the iPhone.
                         double screenHeight = [UIScreen mainScreen].bounds.size.height;
                         double screenWidth = [UIScreen mainScreen].bounds.size.width;
                         
                         // Set the height and content size of the scroll view.
                         CGRect scrollViewFrame = mainScrollView.frame;
                         double initialScrollViewHeight = scrollViewFrame.size.height;
                         mainScrollView.frame = (CGRect){ { scrollViewFrame.origin.x, scrollViewFrame.origin.y }, { screenWidth, screenHeight - KEYBOARD_HEIGHT - NAV_BAR_HEIGHT } };
                         mainScrollView.contentSize = (CGSize){ screenWidth , initialScrollViewHeight };
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)uploadButtonPressed:(id)sender {
    NSString *title = [titleTextField text];
    NSString *description = [descriptionTextView text];
    NSArray *tags = [[tagsTextField text] componentsSeparatedByString:@","];
    BOOL share = [shareSwitch isOn];
    
    if ([title isEqualToString:@""]) {
        titleTextField.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.3];
        [titleTextField becomeFirstResponder];
        return;
    }
    
    // Disable text fields.
    [titleTextField setEnabled:NO];
    [descriptionTextView setEditable:NO];
    [tagsTextField setEnabled:NO];
    
    [uploadButton setHidden:YES];
    [uploadProgressView setHidden:NO];
    
    [self.uploadDelegate uploadVideoWithTitle:title andDescription:description andTags:tags andShare:share];
}

- (void)updateVideoUploadProgressWithNumRead:(unsigned long long)numBytesRead andDataLength:(unsigned long long)dataLength {
    [uploadProgressView setProgress:((float)numBytesRead / (float)dataLength)];
}

@end
