//
//  main.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
