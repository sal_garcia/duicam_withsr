//
//  FeedbackViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/23/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FeedbackViewController : UIViewController {
	// IBOutlets
	IBOutlet UILabel *titleLabel;
	IBOutlet UILabel *descriptionLabel;
	IBOutlet UIImageView *checkmarkImageView;
	IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (void)setTitle:(NSString *)title andDescription:(NSString *)description;
- (void)setTitle:(NSString *)title;
- (void)setDescription:(NSString *)description;
- (void)startAnimating;
- (void)stopAnimating;
- (void)checkAndFade;

@end
