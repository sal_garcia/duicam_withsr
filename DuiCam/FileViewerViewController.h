//
//  FileViewerViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerViewController.h"
#import "ScreenshotViewerViewController.h"

@interface FileViewerViewController : UINavigationController <UITableViewDelegate, UITableViewDataSource>

@end
